'use strict';

module.exports = function (app) {
  app.use('/api/connect', require('../app/connectwise'));
  app.use('/api/freshservice', require('../app/freshservice'));
  app.use('/api/settings', require('../app/commons'));
  app.use('/api/webhook', require('../app/webhook'));
};
