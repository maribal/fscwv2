CREATE DATABASE  IF NOT EXISTS `alithya` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `alithya`;


DROP TABLE IF EXISTS `ticket_entity_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_entity_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_mapping_id` int(6) NOT NULL,
  `entity_type` varchar(50) NOT NULL,
  `freshservice_id` bigint(20) DEFAULT NULL,
  `connectwise_id` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_TicketEntityMappingID` (`ticket_mapping_id`),
  CONSTRAINT `FK_TicketEntityMappingID` FOREIGN KEY (`ticket_mapping_id`) REFERENCES `ticket_id_mappings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_entity_mappings`
--

LOCK TABLES `ticket_entity_mappings` WRITE;
/*!40000 ALTER TABLE `ticket_entity_mappings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_entity_mappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_id_mappings`
--

DROP TABLE IF EXISTS `ticket_id_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_id_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freshservice_ticket_id` int(11) NOT NULL,
  `connectwise_ticket_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


# 12 Sep 2018 - Edited by Sagar
ALTER TABLE alithya.ticket_entity_mappings MODIFY COLUMN connectwise_id VARCHAR(255) DEFAULT NULL;
ALTER TABLE alithya.ticket_entity_mappings MODIFY COLUMN freshservice_id VARCHAR(255) DEFAULT NULL;


# 4 Oct - Edited by Sagar
CREATE TABLE `ticket_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freshdesk_ticket_id` bigint(20) NOT NULL,
  `status_by_cw` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `KEY_FS_TICKET_ID` (`freshdesk_ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

ALTER TABLE ticket_data ADD COLUMN `property_name` varchar(255) NOT NULL DEFAULT 'status' AFTER `freshdesk_ticket_id`;
ALTER TABLE ticket_data MODIFY COLUMN `status_by_cw` TO "property_value_by_cw";