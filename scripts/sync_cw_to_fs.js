const path = require('path');

global.env = process.env.NODE_ENV || 'production';
global.appRoot = path.resolve(__dirname, './../');

//db setup
const mysql_conn = require('./../dbmanager/mysql/connection.js');
mysql_conn.setup();

const DataTransform = require('node-json-transform').DataTransform;

const TicketIdMappingService = require('./../app/webhook/service/TicketIdMappingService');
const FreshService = require('./../app/freshservice/service/FreshService');
const ConnectWebhookService = require('./../app/webhook/service/ConnectWebhookService');
const FreshConfigService = require('./../app/freshservice/service/FreshConfigService');
const EntityDataService = require('./../app/commons/service/EntityDataService');
const TicketDataService = require('./../app/commons/service/TicketDataService');
const CwConfigService = require('./../app/connectwise/service/ConnectWiseConfigService');
const cwTicketConn = CwConfigService.getTicketingConn();
const CwService = require('./../app/connectwise/service/ConnectWiseService');

const WAITING_FOR_APPROVAL = 'Waiting For Approval';
const SERVICE_REQUEST = 'Service Request';

const cwTicketId = process.argv.length >= 3 ? process.argv[2] : null;
const number = process.argv.length >= 4 ? process.argv[3] : null;
const type = process.argv.length >= 5 ? process.argv[4] : null;
const test = process.argv.length >= 6 ? process.argv[5] : null;
const stats = {
    tickets: {},
    final: {
        found_cw_tickets: 0,
        created_fs_tickets: 0,
        updated_fs_tickets: 0,
        update_status_db: 0,
        update_priority_db: 0,
        update_cw_tickets_ext_id: 0,
        synched_conversions: 0,
        synched_time_entries: 0,
        synched_documents: 0,
        synched_child_tickets: 0,
        deleted_fs_tickets: 0,
        full: []
    }
};

const sync = async(ticket = {}, fsTicketId = {}, ticketMapping = {}) => {
    const ticketIndex = `ticket_${ ticket.id }`;

    // update ticket status
    const updateTicketStatus = await ConnectWebhookService.updateTicketStatusInDb(fsTicketId, ticket.status);

    if (updateTicketStatus) {
        stats.tickets[ticketIndex].logs.push(`Update FS ticketId=${ fsTicketId } status=${ JSON.stringify(ticket.status) } in DB`);
        console.log(`Update FS ticketId=${ fsTicketId } status=${ JSON.stringify(ticket.status) } in DB`);

        stats.final.update_status_db++;
    }

    // update ticket priority
    const updateTicketPriority = await ConnectWebhookService.updateTicketPriorityInDb(fsTicketId, ticket.priority);

    if (updateTicketPriority) {
        stats.tickets[ticketIndex].logs.push(`Update FS ticketId=${ fsTicketId } priority=${ JSON.stringify(ticket.priority) } in DB`);
        console.log(`Update FS ticketId=${ fsTicketId } priority=${ JSON.stringify(ticket.priority) } in DB`);

        stats.final.update_priority_db++;
    }

    if (ticket.status && ticket.status.name && !ticket.status.name.toLowerCase().includes('closed')) {
        // update externalId
        const updateExternalId = await ConnectWebhookService.updateExternalId(ticket, fsTicketId);

        if (updateExternalId) {
            stats.tickets[ticketIndex].logs.push(`Update CW ticketId=${ ticket.id } external id`);
            console.log(`Update CW ticketId=${ ticket.id } external id`);

            stats.final.update_cw_tickets_ext_id++;
        }
    }

    let syncedConvos = await EntityDataService.getConnectCreatedConvos(ticket.id);

    stats.tickets[ticketIndex].logs.push(`Conversations found for CW ticketId=${ ticket.id }, convs=${ syncedConvos.join(', ') }`);
    console.log(`Conversations found for CW ticketId=${ ticket.id }`, syncedConvos);

    const syncConversations = await ConnectWebhookService.syncConversations(ticket.id, ticketMapping, syncedConvos);

    if (syncConversations && syncConversations.length) {
        stats.tickets[ticketIndex].logs.push(`Sync conversations for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncConversations=${ syncConversations.join(', ') }`);
        console.log(`Sync conversations for CW ticketId=${ ticket.id }`, ticketMapping, syncConversations);

        stats.final.synched_conversions += syncConversations.length;
    } else {
        stats.tickets[ticketIndex].logs.push(`Do not sync conversations for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncConversations=${ syncConversations.join(', ') }`);
        console.log(`Do not sync conversations for CW ticketId=${ ticket.id }`, ticketMapping, syncConversations);
    }

    const syncDocuments = await ConnectWebhookService.syncDocuments(ticketMapping[0], ticketMapping[1], ticket.id, syncedConvos);

    if (syncDocuments && syncDocuments.length) {
        stats.tickets[ticketIndex].logs.push(`Sync documents for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncDocuments=${ syncDocuments.join(', ') }`);
        console.log(`Sync documents for CW ticketId=${ ticket.id }`, ticketMapping, syncDocuments);

        stats.final.synched_documents += syncDocuments.length;
    } else {
        stats.tickets[ticketIndex].logs.push(`Do not sync documents for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncDocuments=${ syncDocuments.join(', ') }`);
        console.log(`Do not sync documents for CW ticketId=${ ticket.id }`, ticketMapping, syncDocuments);
    }

    const syncTimeEntries = await ConnectWebhookService.syncTimeEntries(ticketMapping[0], ticketMapping[1], ticket.id, syncedConvos);

    if (syncTimeEntries && syncTimeEntries.length) {
        stats.tickets[ticketIndex].logs.push(`Sync time entries for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncTimeEntries=${ syncTimeEntries.join(', ') }`);
        console.log(`Sync time entries for CW ticketId=${ ticket.id }`, ticketMapping, syncTimeEntries);

        stats.final.synched_time_entries += syncTimeEntries.length;
    } else {
        stats.tickets[ticketIndex].logs.push(`Do not sync time entries for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncTimeEntries=${ syncTimeEntries.join(', ') }`);
        console.log(`Do not sync time entries for CW ticketId=${ ticket.id }`, ticketMapping, syncTimeEntries);
    }

    if (ticket.hasChildTicket) {
        const syncChildTickets = await ConnectWebhookService.syncChildTickets(ticketMapping[0], ticketMapping[1], ticket.id, syncedConvos);

        if (syncChildTickets && syncChildTickets.length) {
            stats.tickets[ticketIndex].logs.push(`Sync child tickets for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncChildTickets=[${ syncTimeEntries.join(', ') }]`);
            console.log(`Sync child tickets for CW ticketId=${ ticket.id }`, ticketMapping, syncChildTickets);

            stats.final.synched_child_tickets += syncChildTickets.length;
        } else {
            stats.tickets[ticketIndex].logs.push(`Do not sync child tickets for CW ticketId=${ ticket.id } ticketMapping${ ticketMapping.toString() }, syncChildTickets=[${ syncTimeEntries.join(', ') }]`);
            console.log(`Do not sync child tickets for CW ticketId=${ ticket.id }`, ticketMapping, syncChildTickets);
        }
    }
};

(async() => {
    try {
        const connectwiseAgentId = await FreshService.getMemberIdByEmail( FreshConfigService.getDefaultAgentEmail() );
        global.defaultAgentId = connectwiseAgentId;
        console.log('Default Agent Id:: ', defaultAgentId);

        const unknownGroupId = await FreshService.getGroupIdByName( FreshConfigService.getUnknownGroupName() );
        global.unknownGroupId = unknownGroupId;
        console.log('Unknown Group Id:: ', global.unknownGroupId);

        const ticketProperties = await FreshService.getTicketFields();
        global.statusPropsMap = FreshService.getPropertiesMap(ticketProperties, 'status');
        global.ticketTypePropsMap = FreshService.getPropertiesMap(ticketProperties, 'ticket_type');
        global.priorityPropsMap = FreshService.getPropertiesMap(ticketProperties, 'priority');

        global.connectPrioritiesMap = await CwService.getPriorityMap();

        console.log('Freshservice properties are fetched too! Application is ready to serve...');

    } catch (e) {
        console.log('Error occurred while fetching API user id in app.js', e);
    }

    try {
        let conditions = '';

        if (cwTicketId) {
            conditions = `id=${ cwTicketId } AND board/id=144`;
        }

        if (cwTicketId && cwTicketId == 'last' && number) {
            let diff = 0;

            if (type && type === 'minutes') {
                diff = 1000 * 60 * number;
            }

            if (type && type === 'hours') {
                diff = 1000 * 60 * 60 * number;
            }

            if (type && type === 'days') {
                diff = 1000 * 60 * 60 * 24 * number;
            }

            if (diff) {
                conditions = `dateEntered>=[${ new Date(Date.now() - diff).toISOString() }] AND board/id=144`;
            } else {
                throw 'Something\'s wrong with conditions';
            }
        }

        const pageSize = 50
        let stop = false;
        let page = 1;
        let count = 0;

        const countTickets = await cwTicketConn.getTicketsCount({
            conditions
        });

        console.log(`Found ${ countTickets.count } ticket/s for conditions=${ conditions }`);

        stats.final.found_cw_tickets = countTickets.count;

        while(!stop) {
            const tickets = await cwTicketConn.getTickets({
                conditions,
                page,
                pageSize
            });

            console.log(`Get page=${ page }, pageSize=${ pageSize }, tickets=${ tickets.length }`);

            for (const ticket of tickets) {
                const ticketIndex = `ticket_${ ticket.id }`;
                stats.tickets[ticketIndex] = {
                    id: ticket.id,
                    logs: []
                };

                console.log(`Begin CW ticketId=${ ticket.id }`);

                let ticketMapping = await TicketIdMappingService.getTicketMapping('connectwise', ticket.id);

                // create
                if (!ticketMapping) {
                    stats.final.full.push(`${ ticket.id }, null, ${ ticket.summary }, ${ ticket.owner ? ticket.owner.identifier : null }, CREATE`);

                    if (test) {
                        continue;
                    }

                    console.log(`Create method for CW ticketId=${ ticket.id }, null, ${ ticket.subject }`);

                    let freshServicePayload = await ConnectWebhookService.transformToFreshCreate(ticket, true);
                    freshServicePayload =  CwConfigService.addCWTicketIdToFreshPayload(freshServicePayload, ticket.id, true);

                    if (freshServicePayload.helpdesk_ticket.due_by &&
                        freshServicePayload.helpdesk_ticket.created_at &&
                        new Date(freshServicePayload.helpdesk_ticket.due_by).getTime() < new Date(freshServicePayload.helpdesk_ticket.created_at).getTime()) {
                        const getDateSep = freshServicePayload.helpdesk_ticket.created_at.split('T');
                        freshServicePayload.helpdesk_ticket.due_by = `${ getDateSep[0] }T23:59:00Z`;
                    }

                    const freshServiceTicket = await FreshService.createTicket(freshServicePayload);

                    if (freshServiceTicket && !freshServiceTicket.errors){
                        const createdFreshTicketId = freshServiceTicket.item.helpdesk_ticket.display_id;

                        stats.tickets[ticketIndex].logs.push(`Created FS ticketId=${ createdFreshTicketId }`);

                        console.log(`Created FS ticketId=${ createdFreshTicketId }`);

                        stats.final.created_fs_tickets++;

                        await TicketIdMappingService.createTicketMapping(createdFreshTicketId, ticket.id);

                        stats.tickets[ticketIndex].logs.push(`Link FS ticketId=${ createdFreshTicketId } with CW ticketId=${ ticket.id } in DB`);

                        console.log(`Link FS ticketId=${ createdFreshTicketId } with CW ticketId=${ ticket.id }`);

                        ticketMapping = await TicketIdMappingService.getTicketMapping('connectwise', ticket.id);

                        await sync(ticket, createdFreshTicketId, ticketMapping);
                    } else {
                        stats.tickets[ticketIndex].logs.push(`Something's wrong on creating FS ticket for CW ticketId=${ ticket.id }, error=${ JSON.stringify(freshServiceTicket) }`);
                        console.log(`Something's wrong on creating FS ticket for CW ticketId=${ ticket.id }`, freshServiceTicket);
                    }
                } else {
                    const freshTicketId = ticketMapping[1];

                    console.log(`Found FS ticketId=${ freshTicketId }`);

                    // update
                    console.log(`Update method for CW ticketId=${ ticket.id }`);

                    const freshServiceTicket = await FreshService.getTicket(freshTicketId);
                    const currentTicketType = freshServiceTicket.ticket.type;
                    const freshFieldsToBeUpdated = await ConnectWebhookService.transformToFreshUpdate(ticket, true, freshServiceTicket);

                    stats.final.full.push(`${ ticket.id }, ${ freshServiceTicket ? freshTicketId : null }, ${ ticket.summary }, ${ ticket.owner ? ticket.owner.identifier : null }, UPDATE`);

                    if (test) {
                        continue;
                    }

                    if (ticket.status && ticket.status.name && ticket.status.name == WAITING_FOR_APPROVAL && currentTicketType != SERVICE_REQUEST) {
                        freshFieldsToBeUpdated['helpdesk_ticket']['ticket_type'] = SERVICE_REQUEST;
                    }

                    if (freshFieldsToBeUpdated['helpdesk_ticket']['due_by'] && freshServiceTicket.ticket.fr_due_by){
                        freshFieldsToBeUpdated['helpdesk_ticket']['frDueBy'] = freshServiceTicket.ticket.fr_due_by;
                    }

                    if (freshFieldsToBeUpdated.helpdesk_ticket.subject !== freshServiceTicket.ticket.subject ||
                        freshFieldsToBeUpdated.helpdesk_ticket.impact !== freshServiceTicket.ticket.impact ||
                        freshFieldsToBeUpdated.helpdesk_ticket.urgency !== freshServiceTicket.ticket.urgency ||
                        freshFieldsToBeUpdated.helpdesk_ticket.status !== freshServiceTicket.ticket.status ||
                        freshFieldsToBeUpdated.helpdesk_ticket.ticket_type !== freshServiceTicket.ticket.type ||
                        freshFieldsToBeUpdated.helpdesk_ticket.priority !== freshServiceTicket.ticket.priority ||
                        // freshFieldsToBeUpdated.helpdesk_ticket.due_by !== freshServiceTicket.ticket.due_by ||
                        (freshFieldsToBeUpdated.helpdesk_ticket.custom_field.estimated_start_date_121504 &&
                            freshFieldsToBeUpdated.helpdesk_ticket.custom_field.estimated_start_date_121504 !== freshServiceTicket.ticket.custom_fields.estimated_start_date) ||
                        (freshFieldsToBeUpdated.helpdesk_ticket.custom_field.subtype_121504 &&
                            freshFieldsToBeUpdated.helpdesk_ticket.custom_field.subtype_121504 !== freshServiceTicket.ticket.custom_fields.subtype) ||
                        (freshFieldsToBeUpdated.helpdesk_ticket.custom_field.cw_agent_121504 &&
                            freshFieldsToBeUpdated.helpdesk_ticket.custom_field.cw_agent_121504 !== freshServiceTicket.ticket.custom_fields.cw_agent)) {
                        const updatedTicket = await FreshService.updateTicketV1(freshTicketId, freshFieldsToBeUpdated);

                        if (updatedTicket) {
                            stats.tickets[ticketIndex].logs.push(`Update details for FS ticketId=${ freshTicketId }`);
                            console.log(`Update details for FS ticketId=${ freshTicketId }`);

                            stats.final.updated_fs_tickets++;
                        }
                    } else {
                        stats.tickets[ticketIndex].logs.push(`Do not update details for FS ticketId=${ freshTicketId }`);
                        console.log(`Do not update details for FS ticketId=${ freshTicketId }`);
                    }

                    await sync(ticket, freshTicketId, ticketMapping);
                }

                console.log(`End CW ticketId=${ ticket.id }`);
            }

            count += tickets.length;

            console.log(page, tickets.length, count);

            // stop the script
            if (tickets.length < pageSize) {
                stop = true;
            }

            page++;

            console.log('------------------------------------------');
            console.log('-------------RUNNING STATS----------------');
            console.log('------------------------------------------');
            console.log(JSON.stringify(stats, undefined, 4));

            console.log('WAIT', 'for 3 seconds');
            await new Promise(r => setTimeout(r, 3000));
        }
    } catch (err) {
        console.log(err);
    }

    console.log('------------------------------------------');
    console.log('-----------------STATS--------------------');
    console.log('------------------------------------------');
    console.log(JSON.stringify(stats, undefined, 4));

    // close
    process.exit(0);
})();