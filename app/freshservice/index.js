'use strict';

let express = require('express');
let router = express.Router();
let FreshController = require('./controller/FreshController')

router.get('/ticket', FreshController.fetchTicket);
router.get('/ticket/convos', FreshController.getConversations);
router.get('/ticket/items', FreshController.getServiceItem);

module.exports = router;