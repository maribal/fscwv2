'use strict';

let { success, error } = require("../../../utils/ResponseWrapperUtils");
let { notNull, isNull } = require("../../../utils/ObjectUtils");
let freshService = require("../service/FreshService");

let FreshController = {

  fetchTicket: (req, res) => {
  	return freshService.getTicket(req.query.id || 1).then((output)=>{
  		return success(res, output)
  	}).catch(err=>{
  		console.log("Error occurred while fetching ticket.", err);
  		if(err && err.code){
  			return error(res, err, err.code, err.message);
  		}
  		return error(res, err);
  	});
  },

  getConversations: (req, res) => {
    let id = 176;
    let obj = {
                "helpdesk_ticket": { 
                  // "ticket_type":"Service Request"
                  "ticket_type":"Incident"
                }
              };
    return freshService.updateTicketV1(id, obj).then((output)=>{
      return success(res, output)
    }).catch(err=>{
      console.log("Error occurred while creating ticket.", err);
      if(err && err.code){
        return error(res, err, err.code, err.message);
      }
      return error(res, err);
    });
  },

  getServiceItem: async (req, res) =>{
    let items = await freshService.getRequestedItemDetail(req.query.id);
    return success(res, items);
  }

};

module.exports = FreshController;