
const momentz = require("moment-timezone")
const moment = require('moment');
const freshConfig = require("../../../config/"+global.env+"/freshservice.config.json");
let connectConfig = require("../../../config/"+global.env+"/connectwise.config.json");
let statusKeys = Object.keys(connectConfig.cw_fs_status_map);
// let fsCwStatusMap = {};
// for(let i =0;i<statusKeys.length; i++){
// 	let newKey = connectConfig.cw_fs_status_map[statusKeys[i]];
// 	if(!(newKey in fsCwStatusMap)){
// 		fsCwStatusMap[newKey]	= statusKeys[i];	
// 	}
	
// }

let FreshConfigService = {
	
	getServer: function(){
		return freshConfig.server;
	},

	getApiKey: function(){
		return freshConfig.apiKey;
	},

	getApiUser: function(){
		return freshConfig.apiUser;
	},

	getDefaultAgentEmail: function(){
		return freshConfig.connectwiseAgent;
	},

	getUnknownGroupName: function(){
		return freshConfig.unknownGroupName;	
	},

	getCwStatus: function(val){
		return freshConfig.fs_cw_status_map[val];
	},

	getCwPriority: function(val){
		return freshConfig.fs_cw_priority_map[val];
	},

	getCwType: function(val){
		return freshConfig.fs_cw_type_map[val];
	},

	getBlockedProperties: function(){
		return freshConfig.blockedProperties;
	},

	getAllowedProperties: function(){
		return freshConfig.allowedProperties;
	},

	getCreatePayloadMap: function(){
		// return freshConfig.freshToConnect.create2;
		return {
		    list : '',
		    item: freshConfig.freshToConnect.create,
		    operate: [
		        {
		            run: function(val) { 
									if(val){
										return val.slice(0, 100); 
									}
									return val;
								}, on: "summary"
		        },
		        {
		            run: function(val) { 
		            	if(val){
										val = val.replace("GMT ", "");
										let m1 = moment(val, 'ddd, D MMM, YYYY at h:mm A Z').format("YYYY-MM-DDT00:00:00")+"Z";
		            		return m1
		            	}
		            	return null;
		            }, on: "estimatedStartDate"
						},
						{
							run: function(val) { 
								if(val){
									val = val.replace("GMT ", "");
									let m1 = moment(val, 'ddd, D MMM, YYYY at h:mm A Z').format("YYYY-MM-DDT00:00:00")+"Z";
									return m1;
								}
								return null;
							}, on: "requiredDate"
					},
		        {
		            run: function(team) { 
		            	if(team && team.name && team.name.length>0){
		            		return { "name" : team.name };
		            	}
		            	return null;
		            }, on: "team"
		        },
		        {
		        	run: function(status) { 
		            	if(status && status.name){
		            		let cwStatus = freshConfig.fs_cw_status_map[status.name];
		            		if(cwStatus){
		            			return { "name" : cwStatus };
		            		}
		            	}
		            	return null;
		            }, on: "status"
		        },
		        {
		        	run: function(priority) { 
		            	if(priority && priority.name){
		            		let cwPriority = freshConfig.fs_cw_priority_map[priority.name];
		            		if(cwPriority){
		            			return { "name" : cwPriority };
		            		}
		            	}
		            	return null;
		            }, on: "priority"
		        }
		    ],
		    each: function(item){
		        // make changes
		        item.recordType = "ServiceTicket";
		        item.company = {"identifier": "ALITHYA"}
		        if(item.status){
		        	if(item.status.name == "Unknown"){
		        		delete item.status;	
		        	}
		        }else{
		        	item.status = {"name": "Unknown"};
						}
						
						if(!item.priority.name){
							delete item.priority;
						}
		        
		        return item;
		    }
		};
	},

	isValidTrigger: function(eventPerformedBy){
		return eventPerformedBy !== freshConfig.apiUser;
	},

	getFileNamePrefix: function(){
		return "FRESHRlJFU0hERVNL_";
	},

	getChildMergeNoteRegex: function(){
		return new RegExp(freshConfig.mergeChildNoteRegex, "i");
	},

	getParentMergeNoteRegex: function(){
		return new RegExp(freshConfig.mergeParentNoteRegex, "i");
	},

	getParentMergeNoteDeltaRegex: function(){
		return new RegExp(freshConfig.mergeParentNoteDeltaRegex, "i");
	},

	getV1CustomField: function(fieldKey){
		return (fieldKey+freshConfig.v1CustomFieldSuffix);
	}

}

module.exports = FreshConfigService;
