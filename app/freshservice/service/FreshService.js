let Promise   = require('bluebird');
let Freshdesk = require('freshdesk-api');
const unirest = require('unirest');
const request = require('request');
const fs = require('fs');

let freshConfigService = require("./FreshConfigService");
let CwConfigService = require("../../connectwise/service/ConnectWiseConfigService");
const FileUtils = require("../../../utils/FileUtils");

let asyncFreshdesk = Promise.promisifyAll(
    new Freshdesk(freshConfigService.getServer(), freshConfigService.getApiKey())
);

let FreshService = {

	getAuthHeaders: function(){
		let auth = "Basic " + Buffer.from(freshConfigService.getApiKey() + ":" + 'X').toString("base64");
		return auth;
	},

	makeRequest: function (method, urlPath, qs, data) {		// eslint-disable-line max-params
		let auth = this.getAuthHeaders();
		let hostname = freshConfigService.getServer();
		return new Promise(function(resolve, reject){
			const options = {
				method: method,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': auth
				},
				url: hostname+"/"+urlPath,
				qs: qs
			}
			// console.log("URL:: ", options.url);
			if(data) {
				options.body = JSON.stringify(data)
			}

			console.log(data);
			request(options, function(error, response, body){
				if (error) {
					return reject(error);
				}

				try {
					return resolve(JSON.parse(body))
				} catch(err) {
					console.log('Freshservice, makeRequest, error', data, err);

					return reject(err);
				}
				// return resolve(JSON.parse(body));
			})
		});
	},

	getTicket: async function(ticketId){
		return await asyncFreshdesk.getTicketAsync(ticketId);
	},

	deleteTicket: async function(ticketId){
		return await asyncFreshdesk.deleteTicketAsync(ticketId);
	},

	getTicketFields: async function(){
		let ticketFields = await this.makeRequest('GET', `ticket_fields.json`);
		return ticketFields;
	},

	getTicketFieldData: function(ticketFields, fieldName){
		//assuming ticketFields is an array and fieldName is a string - name of the field which we are looking for
		let output = null;
		ticketFields.forEach(function(ticketField) {
			if(ticketField.ticket_field && ticketField.ticket_field.name==fieldName){
				output =  ticketField;
			}
		});
		return output;
	},

	getPropertiesMap: function(ticketFields, key){
		if(ticketFields && ticketFields.length>0){
			let ticketField = this.getTicketFieldData(ticketFields, key);
			if(ticketField){
				let choices = ticketField.ticket_field.choices;
				let output = {};
				choices.forEach(function(choice){
					output[choice[0]] = choice[1];
				});
				return output;
			}
		}
		return null;
	},

	getChildTickets: async function(ticketId){
		let ticket = await this.makeRequest('GET', `api/v2/tickets/${ticketId}?include=related_tickets`);
		if(ticket && ticket.ticket){
			if(Object.keys(ticket.ticket.related_tickets).length > 0){
				return ticket.ticket.related_tickets['child_ids'];
			}
		}
		return null;
	},

	getParentTicket: async function(ticketId){
		let ticket = await this.makeRequest('GET', `api/v2/tickets/${ticketId}?include=related_tickets`);
		if(ticket && ticket.ticket){
			if(Object.keys(ticket.ticket.related_tickets).length > 0){
				return ticket.ticket.related_tickets['parent_id'];
			}
		}
		return null;
	},

	createTicket : async function(ticketBody){
		try {
			let createdTicket = await this.makeRequest('POST', `helpdesk/tickets.json`, null, ticketBody);
			return createdTicket;
		} catch(err){
			console.log("Exception occurred: ", err);
		}
		return null;
	},

	updateTicket : async function(ticketId, updateObject){
		try{
			let ticket = await asyncFreshdesk.updateTicketAsync(ticketId, updateObject);
			return ticket;
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null;
	},

	updateTicketV1 : async function(ticketId, updateObject){
		try{
			let updatedTicket = await this.makeRequest('PUT', `helpdesk/tickets/${ticketId}.json`, null, updateObject);
			return updatedTicket;
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null;
	},

	updateTicketType: async function(ticketId, ticketType, ticketStatus=null){
		try{
			let updateObject = {
                "helpdesk_ticket": {
                  "ticket_type":ticketType
                }
            };
            if(ticketStatus){
            	updateObject.helpdesk_ticket['status'] = ticketStatus;
            }
			let updatedTicket = await this.updateTicketV1(ticketId, updateObject);
			return updatedTicket;
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null;
	},

	getConversations: async function(ticketId){
		let conversations = await asyncFreshdesk.listAllConversationsAsync(ticketId);
		return conversations;
	},

	getMemberByEmail: async function(memberEmail){
		try{
			let members = await this.makeRequest('GET', `agents.json?query=email is ${memberEmail}`);
			if(members && members.length>0 && members[0].agent){
				return members[0].agent;
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null
	},

	getMemberIdByEmail: async function(memberEmail){
		try{
			let member = await this.getMemberByEmail(memberEmail);
			if(member){
				return member.user_id;
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null
	},

	getGroupIdByName: async function(groupName){
		try{
			let groups = await this.makeRequest('GET', `groups.json`);
			if(groups && groups.length>0){
				for(var i=0; i<groups.length; i++){
					let group = groups[i].group;
					if(group.name===groupName){
						return group.id;
					}
				}
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null;
	},

	getRequestedItemDetail: async function(ticketId){
		try{
			let items = await this.makeRequest('GET', `helpdesk/tickets/${ticketId}/requested_items.json`);
			if(items && items.length>0){
				return items;
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return [];
	},

	getRequesterByEmail: async function(requesterEmail){
		try{
			let requesters = await this.makeRequest('GET', `itil/requesters.json?query=email is ${requesterEmail}`);
			// console.log(requesters);
			if(requesters && requesters.length) {
				return requesters[0].user;
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null
	},

	getRequesterEmailById: async function(requesterId){
		try{
			let requester = await this.makeRequest('GET', `itil/requesters/${requesterId}.json`);
			if(requester && requester.user){
				return requester.user['email'];
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null
	},

	createRequester: async function(name, email, mobile){
		try{
			let payload = {
				"user":{
					"name":name,
					"mobile": mobile,
					"email":email
				}
			}
			let requester = await this.makeRequest('POST', `itil/requesters.json`, null, payload);

			// // BEGIN test
			// try {
			// 	console.log("\n\n Freshservice, createRequester\n\n");
			// 	console.log(JSON.stringify(requester));
			// } catch (err2) {}
			// // END test

			if(requester && requester.user){
				return requester.user;
			}
		}catch(err){
			console.log("Exception occurred: ", err);
		}
		return null
	},

	createRequesterIfNotExist: async function(name, email, mobile){
		let requester = null;
		if(name && email && mobile){
			requester = await FreshService.getRequesterByEmail(email);

			// // BEGIN test
			// try {
			// 	console.log("\n\n Freshservice, createRequesterIfNotExist, getRequesterByEmail\n\n");
			// 	console.log(JSON.stringify(requester));
			// } catch (err2) {}
			// // END test

			if(!requester){
				requester = await FreshService.createRequester(name, email, mobile);
				console.log("Requester created");
			}
		}
		return requester;
	},

	addNote : async function(fsTicketId, body, isPrivate, attachments, userId){
		try{
			var headers = {
				  'Authorization': this.getAuthHeaders(),
				  'Content-Type': 'multipart/form-data'
				};
			let url = freshConfigService.getServer()+"/api/v2/tickets/"+fsTicketId+"/notes";

			var formData = {};
			formData.body = body;
			formData.private = isPrivate;
			formData.incoming = true;

			// add user_id to note
			if (userId) {
				formData.user_id = userId;
			}

			let unr = unirest.post(url)
		  				.headers(headers)
	  					.field(formData);
	  		if(attachments && attachments.length>0){
				for(let i=0; i<attachments.length; i++){
					let attachment = attachments[i];
					if(attachment.name.indexOf("/")<0){ //
						let filePath = global.appRoot+'/dist/File_'+fsTicketId+"_"+i+"_"+attachment.name;
						if(CwConfigService.isConnectWiseUrl(attachment.url)){
							let connectWiseFileHeaders = CwConfigService.getDocumentDownloadHeader();
							await FileUtils.downloadFile(attachment.url, filePath, connectWiseFileHeaders);
						}else{
							await FileUtils.downloadFile(attachment.url, filePath);
						}
						unr = unr.attach('attachments[]', fs.createReadStream(filePath) );
					}
				}
	  	}

			return new Promise(function(resolve, reject){
	  				unr.end(function(response){
	  					return resolve(response.body);
	  				});

			});
		}catch(error){
			console.log("Error occurred while fetching Freshdesk ticket: ", error);
		}
		return null;
	},

	addReply : async function(fsTicketId, body, attachments){
		try{
			var headers = {
				  'Authorization': this.getAuthHeaders(),
				  'Content-Type': 'multipart/form-data'
				};
			let url = freshConfigService.getServer()+"/api/v2/tickets/"+fsTicketId+"/reply";

			var formData = {};
			formData.body = body;
			// formData.from_email = freshConfigService.getApiUser();
			let unr = unirest.post(url)
		  				.headers(headers)
	  					.field(formData);
	  		if(attachments && attachments.length>0){
				for(let i=0; i<attachments.length; i++){
					let attachment = attachments[i];
					let filePath = './dist/File_'+fsTicketId+"_"+i+"_"+attachment.name;
					await FileUtils.downloadFile(attachment.url, filePath);
					unr = unr.attach('attachments[]', fs.createReadStream(filePath) );
				}
	  		}

			return new Promise(function(resolve, reject){
	  				unr.end(function(response){
	  					console.log("response: ", response.body);
	  					return resolve(response.body);
	  				});

			});
		}catch(error){
			console.log("Error occurred while fetching Freshdesk ticket: ", error);
		}
		return null;
	}

}

module.exports = FreshService;
