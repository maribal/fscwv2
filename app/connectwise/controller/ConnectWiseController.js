'use strict';

let { success, error } = require("../../../utils/ResponseWrapperUtils");
let { notNull, isNull } = require("../../../utils/ObjectUtils");
let cwService = require("../service/ConnectWiseService");
let CwConfigService = require("../service/ConnectWiseConfigService");

let ConnectWiseController = {

  fetchTicket: (req, res) => {

  	return cwService.getTicket(req.query.id || 1).then((output)=>{
  		return success(res, output)
  	}).catch(err=>{
  		console.log("Error occurred while fetching ticket.", err);
  		if(err && err.code){
  			return error(res, err, err.code, err.message);
  		}
  		return error(res, err);
  	});
  },
  
  getMembers: async (req, res) => {
    let member = await cwService.getTeam('Unknown');
    // console.log("STATUS:: ",CwConfigService.getStatus("In Progress"));
    return success(res, member);
  },
  

};

module.exports = ConnectWiseController;