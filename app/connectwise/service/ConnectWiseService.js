const request = require('request');
let cwConfigService = require('./ConnectWiseConfigService');
let cwTicketConn = cwConfigService.getTicketingConn();
let cwNoteConn = cwConfigService.getNotesConn();
let cwMemberConn = cwConfigService.getMemberConn();
let cwConn = cwConfigService.getConn();
let cwConfig = require('../../../config/'+global.env+'/connectwise.config.json');
let BASE_API_URL = cwConfigService.getBaseApiUrl();

const ConnectWiseService = {
	getTicket : async function(ticketId){
		try{
			let ticket = await cwTicketConn.getTicketById(ticketId);
			return ticket;
		}catch(err){
			console.log("exception occurred while fetching ticket: ", err);
		}

		return null;
	},

	createTicket : async function (ticketBody){
		try{
			let ticket = await cwTicketConn.createTicket(ticketBody);
			return ticket;
		} catch (error) {
			console.log("Error occurred while creating cw ticket: ", error);
		}

		return null;
	},

	updateTicket : async function(ticketId, operations){
		try{
			if(operations && operations.length > 0){
				return await cwTicketConn.updateTicket(ticketId, operations);
			}else{
				console.log("Empty operations for CwTicketId: ", ticketId);
			}
		} catch (error) {
			console.log("Error occurred while updating cw ticket: ", error);
		}

		return null;
	},

	updateTicketStatus: async function(ticketId, ticketStatus){
		let operation = {
	        "op": 'replace',
	        "path": 'status',
	        "value": {"name": ticketStatus}
	    };

	    return await this.updateTicket(ticketId, [operation]);
	},

	addNote: async function(ticketId, text, isPrivate){
		try{
			let notePayload = {};
			notePayload.ticketId = ticketId;
			notePayload.text = text;
			notePayload.detailDescriptionFlag = (!isPrivate);
			notePayload.internalAnalysisFlag = isPrivate;
			let note = await cwNoteConn.createServiceNote(ticketId, notePayload);
			return note;
		} catch (error) {
			console.log("Error occurred while adding note: ", error);
		}

		return null;
	},

	getNotes: async function(ticketId){
		try{
			let notes = await cwNoteConn.getServiceNotes(ticketId);
			// console.log("notes", notes);
			return notes;
		} catch (error) {
			console.log("Error occurred while fetching notes: ", error);
		}

		return null;
	},

	getPriorityMap: async function(){
		try{
			let priorities = await cwConn.api(BASE_API_URL+"/service/priorities", "GET");
			if(priorities && priorities.length>0){
				let output = {};
				priorities.forEach(function(priority){
					output[priority.name] = priority.id;
				});
				return output;
			}
		} catch (error) {
			console.log("Error occurred while fetching contacts: ", error);
		}

		return {};
	},

	getMember: async function(email){
		try{

			let members = await cwConn.api(BASE_API_URL+"/system/members?conditions=officeEmail like '"+email+"'", "GET");
			if(members && members.length>0){
				return members[0].identifier;
			}
		} catch (error) {
			console.log("Error occurred while fetching members: ", error);
		}

		return null;
	},

	getContact: async function(email){
		try{
			let contacts = await cwConn.api(BASE_API_URL+"/company/contacts?conditions=company/id=3004&childconditions=communicationItems/value like '"+email+"'", "GET");
			if(contacts && contacts.length>0){
				return contacts[0];
			}
		} catch (error) {
			console.log("Error occurred while fetching contacts: ", error);
		}

		return null;
	},

	getMemberById: async function(memberId){
		try{
			let member = await cwMemberConn.getMemberByIdentifier(memberId);

			return member;
		} catch (error) {
			console.log("Error occurred while fetching member by id: ", error);
		}

		return null;
	},

	getTeam: async function(teamName){
		try{
			let teams =  await cwConn.api(BASE_API_URL+"/service/boards/"+cwConfigService.getServiceBoardId()+"/teams?conditions=name like '"+teamName+"'", "GET");
			if(teams && teams.length>0){
				return teams[0];
			}
		} catch (error) {
			console.log("Error occurred while creating cw ticket: ", error);
		}

		return null;
	},

	getMemberEmailById: async function(memberId){
		try{
			let member = await this.getMemberById(memberId);

			if (member && member.officeEmail) {
				return member.officeEmail;
			}
		} catch (error) {
			console.log("Error occurred while creating cw ticket: ", error);
		}

		return null;
	},

	getMemberNameById: async function(memberId){
		try{

			let member = await this.getMemberById(memberId);
			if(member){
				return member.firstName + (member.lastName ? (" "+member.lastName) : "");
			}
		} catch (error) {
			console.log("Error occurred while creating cw ticket: ", error);
		}

		return null;
	},

	getAttachments: async function(cwTicketId) {
		try {
			let attachments = await cwTicketConn.getTicketDocuments(cwTicketId);
			if(attachments && attachments.length>0){
				return attachments;
			}
		} catch (error) {
			console.log("Error occurred while fetching cw attachments: ", error);
		}

		return null;
	},

	addAttachment: async function(cwTicketId, fileDataURI, fileName) {
		try {
			let file = await cwConfigService.getFormDataConn(`${BASE_API_URL}/system/documents`, {
				recordType: 'Ticket',
				title: fileName,
				file: fileDataURI,
				recordId: cwTicketId
			});

			return file;
		} catch (error) {
			console.log("Error occured while adding CW Attachment", error);
		}
	},

	getChildTickets : async function(cwTicketId){
		try {
			let childTickets = await cwConn.api(BASE_API_URL+"/service/tickets/"+cwTicketId+"/tasks?conditions=childTicketId>0", "GET");

			return childTickets;
		} catch (error) {
			console.log("Error occured while fetching CW child tickets", error);
		}
	},

	getConnectWiseData: async function(url){
		try{
			let data =  await cwConn.api(url, "GET");
			return data;
		} catch (error) {
			console.log("Error occurred while get ticket data: ", error);
		}

		return null;
	},

	getTimeEntryNotes: async function(cwTicketId){
		try {
			const url = `${BASE_API_URL}/time/entries?conditions=(chargeToType='ServiceTicket' OR chargeToType='ProjectTicket') AND chargeToId=${cwTicketId} AND notes!=null`;
			const entries = await cwTicketConn.api(url, 'GET');

			console.log('total entries', entries.length);

			return entries;
		} catch (error) {
			console.log("Error occurred while get ticket data: ", error);
		}

		return null;
	}
};

module.exports = ConnectWiseService;
