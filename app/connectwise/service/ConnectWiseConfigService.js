const momentz = require('moment-timezone');
const moment = require('moment');
const btoa = require('btoa');
const request = require('request');
let ConnectWiseRest = require('connectwise-rest');
let cwConfig = require('../../../config/'+global.env+'/connectwise.config.json');
let FreshConfigService = require('../../freshservice/service/FreshConfigService');


const ConnectWiseConfigService = {
	getAuthHeader: function(){
		let authRaw = cwConfig.companyId + '+' + cwConfig.publicKey + ':' + cwConfig.privateKey;

		return ('Basic ' + Buffer.from(authRaw).toString('base64'));
	},

	getLoginFormData: function(){
		let formData = {
			username: cwConfig.username,
			password: cwConfig.password,
			companyname: cwConfig.companyId
		};

		// var formData = querystring.stringify(form);
		return formData;
	},

	getTicketingConn: function(){
		let options = this.getOptions();

	    return new ConnectWiseRest(options).ServiceDeskAPI.Tickets;
	},

	getNotesConn: function(){
		let options = this.getOptions();

	    return new ConnectWiseRest(options).ServiceDeskAPI.ServiceNotes;
	},

	getMemberConn: function(){
		let options = this.getOptions();

	    return new ConnectWiseRest(options).SystemAPI.Members;
	},

	getConn: function(){
		let options = this.getOptions();

	    return new ConnectWiseRest(options).API;
	},

	getDocumentDownloadUrl: function(documentId, documentName){
		let url = `https://${cwConfig.server}/${cwConfig.entryPoint}/services/system_io/FileManagement/FileDownload.aspx?RecordId=${documentId}`;

		return url;
	},

	isConnectWiseUrl: function(url){
		return url.indexOf(cwConfig.server);
	},

	getDocumentDownloadHeader: function(){
		let headers =	{
			'Authorization': this.getAuthHeader(),
			'clientId': cwConfig.clientId || ''
		};

		return headers;
	},

	getFormDataConn: function(url, formData) {
		let headerUser = cwConfig.companyId + '+' + (cwConfig.publicKey || '');
		let headerPass = cwConfig.privateKey;
		let authHeader = 'Basic ' + btoa(headerUser + ':' + headerPass);
		let options = {
			url: url,
			method: 'POST',
			headers: {
				'Content-Type': 'multipart/form-data',
				'Authorization': authHeader,
				'clientId': cwConfig.clientId || ''
			},
			formData: formData
		};

		return new Promise(function(resolve, reject){
			request(options, function (error, response, body) {
				if (error) throw new Error(error);
				console.log('Uploaded attachment on CW');
				return resolve(body);
			});
		});
	},

	getOptions: function(){
		return {
	        companyId: cwConfig.companyId,
	        companyUrl: cwConfig.server,
	        publicKey: cwConfig.publicKey,
			clientId: cwConfig.clientId || '',
	        privateKey: cwConfig.privateKey,
	        entryPoint: cwConfig.entryPoint, // optional, defaults to 'v4_6_release'
	        timeout: cwConfig.timeout_ms,             // optional, request connection timeout in ms, defaults to 20000
	        retry: cwConfig.retry,               // optional, defaults to false
	        debug: cwConfig.retry               // optional, enable debug logging
	    };
	},

	getCompany : function(){
		return {'identifier': cwConfig.company};
	},

	getDefaultTicketType: function(){
		return cwConfig.ticketType;
	},

	getServerUrl: function(){
		return 'https://'+cwConfig.server+'/'+cwConfig.entryPoint;
	},

	getLoginUrl: function(){
		return 'https://'+cwConfig.server+'/'+cwConfig.entryPoint+'/login/login.aspx';
	},

	getConfigs: function(){
		return cwConfig;
	},

	getBaseApiUrl: function(){
		return this.getServerUrl()+'/apis/3.0';
	},

	getServiceBoardId: function(){
		return cwConfig.serviceBoardId || 144;
	},

	getStatus: function(val){
		return global.statusPropsMap[val];
	},

	getFreshServiceStatusStr: function(cwStatus){
		let fsStatus = cwConfig.cw_fs_status_map[cwStatus];
		if(!fsStatus){
			return 'Unknown';
		}else{
			if(fsStatus == 'Unknown'){
				return null;
			}
		}

		return fsStatus;
	},

	getFreshServiceStatus: function(cwStatus){
		if(cwConfig.cw_fs_status_map[cwStatus]){
    		let fsStatus = global.statusPropsMap[ cwConfig.cw_fs_status_map[cwStatus] ];
    		return fsStatus;
    	}

		return  null;
	},

	getFreshServiceTypeStr: function(cwType){
		if(cwConfig.cw_fs_type_map[cwType]){
			return cwConfig.cw_fs_type_map[cwType];
		}

		return 'Incident';
	},

	getFreshServicePriorityStr: function(cwPriority){
		if(cwConfig.cw_fs_priority_map[cwPriority]){
			return cwConfig.cw_fs_priority_map[cwPriority];
		}

		return null;
	},

	getFreshServicePriority: function(cwPriority){
		let priorityStr = this.getFreshServicePriorityStr(cwPriority);
		if(priorityStr){
			return global.priorityPropsMap[priorityStr];
		}

		return null;
	},

	getUpdatePayloadMap: function(){
		return {
		    list : '',
		    item: cwConfig.ConnectToFresh.update,
		    operate: [
		        {
		            run: function(val) {
		            	return ConnectWiseConfigService.getFreshServiceStatus(val);
					}, on: 'status'
				},
				{
		            run: function(val) {
		            	return ConnectWiseConfigService.getFreshServicePriority(val);
					}, on: 'priority'
		        },
		        {
		            run: function(val) { return cwConfig.properties.impact[val] }, on: 'impact'
		        },
		        {
		            run: function(val) { return cwConfig.properties.urgency[val] }, on: 'urgency'
		        },
		        {
		            run: function(val) {
		            	if(val){
		            		return moment(new Date(val)).format('YYYY-MM-DD')
		            	}
		            	return null;
		            }, on: 'custom_fields.estimated_start_date'
				},
				{
		            run: function(val) {
		            	if(val){
					//		return momentz(new Date(val)).clone().tz('Canada/Atlantic').format();
		            		return moment(new Date(val)).format('YYYY-MM-DDT04:00:00')+'Z'
		            	}
		            	return null;
		            }, on: 'due_by'
				},
		    ],
		    each: function(item){
				// make changes
				if(!item.status){
					// delete item.status;
					item['status'] = global.statusPropsMap['Unknown']
				}else{
					if(item['status'] == global.statusPropsMap['Unknown']){
						delete item.status;
					}
				}

				if(!item.priority){
					delete item.priority;
				}

		        return item;
		    }
		};
	},

	getUpdateV1PayloadMap: function(){
		return {
		    list : '',
		    item: cwConfig.ConnectToFresh.updateV1,
		    operate: [
		        {
		            run: function(val) {
		            	return ConnectWiseConfigService.getFreshServiceStatus(val);
					}, on: 'status'
				},
				{
		            run: function(val) {
		            	return ConnectWiseConfigService.getFreshServiceTypeStr(val);
					}, on: 'ticket_type'
				},
				{
		            run: function(val) {
		            	return ConnectWiseConfigService.getFreshServicePriority(val);
					}, on: 'priority'
		        },
		        {
		            run: function(val) { return cwConfig.properties.impact[val] }, on: 'impact'
		        },
		        {
		            run: function(val) { return cwConfig.properties.urgency[val] }, on: 'urgency'
		        },
		        {
		            run: function(val) {
		            	if(val){
		            		return moment(new Date(val)).format('YYYY-MM-DD')
		            	}
		            	return null;
		            }, on: 'custom_field.estimated_start_date_121504'
				},
				{
		            run: function(val) {
		            	if(val){
						//	return momentz(new Date(val)).clone().tz('Canada/Atlantic').format();
		            		return moment(new Date(val)).format('YYYY-MM-DDT04:00:00')+'Z'
		            	}
		            	return null;
		            }, on: 'due_by'
				},
		    ],
		    each: function(item){
				// make changes
				if(!item.status){
					item['status'] = global.statusPropsMap['Unknown']
				}else{
					if(item['status'] == global.statusPropsMap['Unknown']){
						delete item.status;
					}
				}

				if(!item.priority){
					delete item.priority;
				}

		        return item;
		    }
		};
	},

	addCWTicketIdToFreshPayload: function(freshPayload, cwTicketId, isApiV1=false){
		if(isApiV1){
			freshPayload['helpdesk_ticket']['custom_field'][FreshConfigService.getV1CustomField('connectwise_ticket_id')] = String(cwTicketId)	;
		}else{
			freshPayload['custom_fields']['connectwise_ticket_id'] = String(cwTicketId)	;
		}
		return freshPayload;
	}
};

module.exports = ConnectWiseConfigService;
