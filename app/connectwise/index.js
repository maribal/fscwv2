'use strict';

let express = require('express');
let router = express.Router();
let ConnectWiseController = require('./controller/ConnectWiseController')

router.get('/ticket', ConnectWiseController.fetchTicket);
router.get('/ticket/testMember', ConnectWiseController.getMembers);

module.exports = router;