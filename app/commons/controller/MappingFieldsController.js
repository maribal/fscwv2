let MappingFieldsService = require(__dirname+'../../service/MappingFieldsService');
let { success, error } = require("../../../utils/ResponseWrapperUtils");

let fieldMapperObject = [
    {
        "id" : 1,
        "freshdesk_field" : "ticket_id",
        "trac_field" : "vst_case_no"
    },
    {
        "id" : 2,
        "freshdesk_field" : "ticket_subject",
        "trac_field" : "arihant"
    },
    {
        "id" : 3,
        "freshdesk_field" : "ticket_cf_issue_type",
        "trac_field" : "vst_case_type"
    },
    {
        "id" : 4,
        "freshdesk_field" : "ticket_priority",
        "trac_field" : "utkarsh"
    }
];

let MappingFieldsController = {

	showMappedFields: (req, res) => {
		return MappingFieldsService.showFieldMappings().then((output)=>{
			return success(res, output)
		}).catch(err=>{
			console.log("Error occurred while fetching fields.", err);
			if(err && err.code){
				return error(res, err, err.code, err.message);
			}
			return error(res, err);
		});
	},

	createMappingField: (req, res) => {
	  return MappingFieldsService.createFieldMapping(fieldMapperObject).then((output)=>{
		return success(res, output)
	  }).catch(err=>{
		console.log("Error occurred while creating fields.", err);
		if(err && err.code){
		  return error(res, err, err.code, err.message);
		}
		return error(res, err);
	  });
	}

}

module.exports = MappingFieldsController;
