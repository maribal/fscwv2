
let TrackableFieldsService = require(__dirname+'../../service/TrackableFieldsService');
let { success, error } = require("../../../utils/ResponseWrapperUtils");

let fieldObject = {
	'trac' : ['vst_case_no','summary','vst_case_type','vst_case_severity','priority','customer'],
	'freshdesk' : ['ticket_id','ticket_subject','ticket_cf_issue_type','ticket_cf_severity','ticket_cf_priority','ticket_company_name']
};

let isTrackable = 1;

let TrackableFieldsController = {

	showTrackableFields: (req, res) => {
		return TrackableFieldsService.showFields(isTrackable).then((output)=>{
			return success(res, output)
		}).catch(err=>{
			console.log("Error occurred while fetching fields.", err);
			if(err && err.code){
				return error(res, err, err.code, err.message);
			}
			return error(res, err);
		});
	},
	
	createTrackableField: (req, res) => {
	  return TrackableFieldsService.createField(fieldObject).then((output)=>{
		return success(res, output)
	  }).catch(err=>{
		console.log("Error occurred while creating fields.", err);
		if(err && err.code){
		  return error(res, err, err.code, err.message);
		}
		return error(res, err);
	  });
	}

}

module.exports = TrackableFieldsController;
