'use strict';

let express = require('express');
let router = express.Router();
let TrackableFieldsController = require('./controller/TrackableFieldsController');
let MappingFieldsController = require('./controller/MappingFieldsController');

router.get('/trackable_fields', TrackableFieldsController.showTrackableFields);
router.post('/trackable_fields', TrackableFieldsController.createTrackableField);

router.get('/field_mappings', MappingFieldsController.showMappedFields);
router.post('/field_mappings', MappingFieldsController.createMappingField);

module.exports = router;
