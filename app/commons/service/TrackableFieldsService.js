
// let DBConnect = require('./DBConnectionService');
let SourceDataProvider = require('../dataprovider/SourceDataProvider');
let isTrackable = 1;
let Freshdesk = 'freshdesk';
let Trac = 'trac';

let TrackableFieldsService = {

    createField: async function(fieldObject){
		if(fieldObject[Freshdesk].length > 0){
			for(let ii=0; ii<fieldObject[Freshdesk].length; ii++){
				await SourceDataProvider.create(Freshdesk, fieldObject[Freshdesk][ii], isTrackable);
			}
		}
		if(fieldObject[Trac].length > 0){
			for(let jj=0; jj<fieldObject[Trac].length; jj++){
				await SourceDataProvider.create(Trac, fieldObject[Trac][jj], isTrackable);
			}
		}
		return 'Successfully updated the tracking table';
	},
	showFields: async function(isTrackable){
		let fields = {};
		let freshdeskfieldlist = [];
		let tracfieldlist = [];
		let freshdeskfields = await SourceDataProvider.showall(isTrackable, Freshdesk);
		Object.keys(freshdeskfields).forEach(function(key){
			freshdeskfieldlist.push(freshdeskfields[key].field_name);
		});
		let tracfields = await SourceDataProvider.showall(isTrackable, Trac);
		Object.keys(tracfields).forEach(function(key){
			tracfieldlist.push(tracfields[key].field_name);
		});
		fields[Freshdesk] = freshdeskfieldlist;
		fields[Trac] = tracfieldlist;
		return fields;
	}

}

module.exports = TrackableFieldsService;
