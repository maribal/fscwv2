
let TicketDataProvider = require('../dataprovider/TicketDataProvider');

let TicketDataService = {

	create: async function(freshdeskTicketId, propertyName, propertyValue){
		if (freshdeskTicketId && propertyName && propertyValue){
			const output =  await TicketDataProvider.create(freshdeskTicketId, propertyName, propertyValue);

			return output.insertId;
		}

		return null;
	},

	getLastProperty: async function(freshdeskTicketId, propertyName){
		const results =  await TicketDataProvider.getProperty(freshdeskTicketId, propertyName);

		if (results && results.length>0){
			return results[0].propertyValue;
		}

		return null;
	},

	getLastStatus: async function(freshdeskTicketId){
		return (await this.getLastProperty(freshdeskTicketId, 'status'));
	},

	getLastPriority: async function(freshdeskTicketId){
		return (await this.getLastProperty(freshdeskTicketId, 'priority'));
	},

	updateProperty: async function(freshdeskTicketId, propertyName, propertyValue){
		if (freshdeskTicketId && propertyName && propertyValue){
			return await TicketDataProvider.updateProperty(freshdeskTicketId, propertyName, propertyValue);
		}

		return null;
	},

	createOrUpdateStatus: async function(freshdeskTicketId, status){
		const lastStatus = await this.getLastStatus(freshdeskTicketId);

		if (lastStatus){
			return await this.updateProperty(freshdeskTicketId, 'status', status);
		} else{
			return await this.create(freshdeskTicketId, 'status', status);
		}
	},

	createOrUpdatePriority: async function(freshdeskTicketId, priority){
		const  lastPriority = await this.getLastProperty(freshdeskTicketId, 'priority');
		if (lastPriority) {
			return await this.updateProperty(freshdeskTicketId, 'priority', priority);
		} else {
			return await this.create(freshdeskTicketId, 'priority', priority);
		}
	}


};



module.exports = TicketDataService;