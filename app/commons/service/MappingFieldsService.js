
// let DBConnect = require('./DBConnectionService');
let MappingDataProvider = require('../dataprovider/MappingDataProvider');

let MappingFieldsService = {

    createFieldMapping: async function(fieldMapperObject){
		if(fieldMapperObject.length > 0){
			for(let ii=0; ii<fieldMapperObject.length; ii++){
				await MappingDataProvider.replaceMapping(fieldMapperObject[ii]['id'], fieldMapperObject[ii]['freshdesk_field'], fieldMapperObject[ii]['trac_field']);
			}
		}
		return true;
	},

	showFieldMappings: async function(){
		let fields = await MappingDataProvider.showMappings();
		return fields;
	},

	getFreshConnectMapping: async function(){
		let mapping = {};
		let mappedFielddata = await MappingDataProvider.showMappings();
		
		if(mappedFielddata){
			Object.keys(mappedFielddata).forEach(function(key){
				mapping[mappedFielddata[key].freshservice_field] = mappedFielddata[key].connectwise_field;
			});
		}
		return mapping;
	},

	getConnectFreshMapping: async function(){
		let mapping = {};
		let mappedFielddata = await MappingDataProvider.showMappings();
		if(mappedFielddata){
			Object.keys(mappedFielddata).forEach(function(key){
				mapping[mappedFielddata[key].connectwise_field] = mappedFielddata[key].freshservice_field;
			});
		}
		return mapping;
	}

}

module.exports = MappingFieldsService;
