let EntityDataProvider = require('../dataprovider/EntityDataProvider');

let EntityDataService = {

	getFreshCreatedConvos: async function(ticketId){
		let createdConvos = await EntityDataProvider.findEntities("freshservice", ticketId, "conversation");
		console.log("createdConvos", createdConvos[0].data);
		if(createdConvos[0].data){
			let createdIds = createdConvos[0].data.split(",")
			return createdIds;
		}
		return [];
	},

	getConnectCreatedConvos: async function(ticketId){
		const createdConvos = await EntityDataProvider.findEntities("connectwise", ticketId, "conversation");

		if (createdConvos[0].data){
			return createdConvos[0].data.split(",");
		}

		return [];
	},

	addFreshConvo: async function(mappingId, freshserviceId, connectwiseId){
		const output = await EntityDataProvider.create("freshservice", mappingId, "conversation", freshserviceId, connectwiseId);

		return output.insertId;
	},

	addConnectNotes: async function(mappingId, freshserviceId, connectwiseId){
		let output = await EntityDataProvider.create("connectwise", mappingId, "notes", freshserviceId, connectwiseId);
		return output.insertId;
	},

	addFreshAttachments: async function(mappingId, freshserviceId, connectwiseId) {
		let output = await EntityDataProvider.create("freshservice", mappingId, "attachments", freshserviceId, connectwiseId);
		return output.insertId;
	},

	isMergeEntryExist: async function(ticketMappingId, freshserviceId, connectwiseId){
		let fields = await EntityDataProvider.checkMergeEntry(ticketMappingId, freshserviceId, connectwiseId);
		console.log("fields", fields);
		if(fields && fields.length>0){
			return true;
		}
		return false;
	}
};

module.exports = EntityDataService;