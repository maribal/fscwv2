
let db = require('../../../dbmanager/mysql/db')

/*
 * Data provider is the interaction layer for DB. 
 * Only data provider can read/write to DB directly and this will contain all raw queries.
 */

 let MappingDataProvider = {

    replaceMapping: function(id, freshdeskField, tracField){
        let query = "REPLACE INTO field_mappings (id, freshdesk_field, trac_field) VALUES (?, ?, ?) ";
        return db.querySql(query, [id, freshdeskField, tracField]);
    },

    showMappings: function(){
        let query = "SELECT id, freshservice_field, connectwise_field FROM field_mappings ";
        return db.querySql(query);
    }
 }

module.exports = MappingDataProvider
