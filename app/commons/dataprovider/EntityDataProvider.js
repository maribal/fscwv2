
let db = require('../../../dbmanager/mysql/db')
let TicketIdDataProvider = require('../../webhook/dataprovider/TicketIdDataProvider');

/*
 * Data provider is the interaction layer for DB. 
 * Only data provider can read/write to DB directly and this will contain all raw queries.
 */

 let EntityDataProvider = {

    create: function(source, mappingId, entityType, freshserviceId, connectwiseId){
        let query = "INSERT into ticket_entity_mappings (ticket_mapping_id, entity_type, freshservice_id, connectwise_id) values (?, ?, ?, ?);";
        return db.querySql(query, [mappingId, entityType, freshserviceId, connectwiseId]);
    },

    findEntities: function(source, ticketId, entityType){
        const sourceIdColumn = source == 'freshservice' ? 'freshservice_ticket_id' : 'connectwise_ticket_id';
        const sourceEntityIdColumn = source == 'freshservice' ? 'freshservice_id' : 'connectwise_id';
        let query = "SELECT GROUP_CONCAT(tem." + sourceEntityIdColumn + ") AS data FROM ticket_entity_mappings tem INNER JOIN ticket_id_mappings tim ON tem.ticket_mapping_id = tim.id WHERE tim." + sourceIdColumn + " = ? AND entity_type = ?";
        return db.querySql(query, [ticketId, entityType]);        
    },

    checkMergeEntry: function(ticketMappingId, freshserviceId, connectwiseId){
        let query = "SELECT * FROM ticket_entity_mappings WHERE ticket_mapping_id = ? AND (freshservice_id = ? or connectwise_id = ?) ";
        return db.querySql(query, [ticketMappingId, freshserviceId, connectwiseId]);
    },
 }

module.exports = EntityDataProvider