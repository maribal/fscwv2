
let db = require('../../../dbmanager/mysql/db')

/*
 * Data provider is the interaction layer for DB. 
 * Only data provider can read/write to DB directly and this will contain all raw queries.
 */

 let SourceDataProvider = {

    create: function(fieldType, fieldName, isTrackable){
        let query = "REPLACE INTO fields (field_name, field_type, is_trackable) VALUES (?, ?, ?)";
        return db.querySql(query, [fieldName, fieldType, isTrackable]);
    },

    showall: function(isTrackable, fieldType){
        let query = "SELECT field_name FROM fields WHERE is_trackable = ? AND field_type = ? ";
        return db.querySql(query, [isTrackable, fieldType]);
    }
 }

module.exports = SourceDataProvider
