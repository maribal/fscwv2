let db = require('../../../dbmanager/mysql/db')

let TicketDataProvider = {

	create: function(freshdeskTicketId, propertyName, propertyValue){
        const query = "INSERT INTO ticket_data (`freshdesk_ticket_id`,`property_name` ,`property_value_by_cw`) VALUES (?, ?, ?) ";

        return db.querySql(query, [freshdeskTicketId, propertyName, propertyValue]);
    },

    getProperty: function(freshdeskTicketId, propertyName){
        const query = "SELECT property_value_by_cw as propertyValue FROM ticket_data where freshdesk_ticket_id = ? and property_name=?";

        return db.querySql(query, [freshdeskTicketId, propertyName]);
    },
    
    updateProperty: function(freshdeskTicketId, propertyName, propertyValue){
    	const query = "UPDATE ticket_data SET property_value_by_cw = ? where freshdesk_ticket_id = ? and property_name = ?";

    	return db.querySql(query, [propertyValue, freshdeskTicketId, propertyName]);
    }

};

module.exports = TicketDataProvider;