
let TicketIdMappingService = require("./TicketIdMappingService");
let CwConfigService = require("../../connectwise/service/ConnectWiseConfigService");
let CwService = require("../../connectwise/service/ConnectWiseService");
let FreshConfigService = require("../../freshservice/service/FreshConfigService");
let FreshService = require("../../freshservice/service/FreshService");
let MappingFieldsService = require("../../commons/service/MappingFieldsService");
let EntityDataService = require("../../commons/service/EntityDataService");
let TicketDataService = require("../../commons/service/TicketDataService");
let DataTransform = require("node-json-transform").DataTransform;
const FileUtils = require("../../../utils/FileUtils");
const StringUtils = require("../../../utils/StringUtils");
const fs = require('fs');
const inlineAttachmentRegex = /https:\/\/attachment.freshservice.com[^\\"]+/g;

const SERVICE_REQUEST = 'Service Request';

let FreshWebhookService = {

	parseTicketId: function(stringId){
		return parseInt(stringId.split("-")[1])
	},

	getInlineAttachmentUrls: function(htmlContent){
		if(htmlContent){
			return htmlContent.match(inlineAttachmentRegex); // returns null if no url is found
		}
		return null;
	},

	getAttachmentObjectFromUrl: function(url){
		return {
			"content_type": "image/png",
			"size": 10700,
			"inline": true,
			"name": `InlineAttachment_${FreshWebhookService.getRandomNumber(1,100)}${new Date().getTime()}.png`,
			"attachment_url": url,
			"created_at": "2019-05-19T20:12:35Z",
			"updated_at": "2019-05-19T20:12:36Z"
		};
	},

	getRandomNumber: function(min, max){
    	return Math.floor(Math.random() * (+max - +min)) + +min;
	},

	syncableTicket: function(reqBody){
		let blockedProps = FreshConfigService.getBlockedProperties();
		let allowedProps = FreshConfigService.getAllowedProperties();
		if(blockedProps.problemType.indexOf(reqBody.problemType)<0 && allowedProps.group.indexOf(reqBody.group_name)>=0){
			return true;
		}
		return false;
	},

	createTicket: async function(reqBody){
		let fsTicketId = this.parseTicketId(reqBody.id);
		let freshTicket = null;
		let alreadyPresentTicketId = await TicketIdMappingService.getConnectTicketId(fsTicketId);
		if(!this.syncableTicket(reqBody)){
			console.log(`The ticket doesn't meet required criteria for syncing tickets. Problem Type: ${reqBody.problemType}, Group Name: ${reqBody.group_name}`);
			return null;
		}
		console.log(`Create ticket in CW. FS ticket id ${fsTicketId}. CwTicketId: ${alreadyPresentTicketId}`);
		if(!alreadyPresentTicketId) {
			let cwTicketBody = null;
			let inlineAttachmentUrls = this.getInlineAttachmentUrls(reqBody.description_html);
			let attachments = [];
			if(inlineAttachmentUrls && Array.isArray(inlineAttachmentUrls)){
				inlineAttachmentUrls.forEach((inlineUrl)=>{
					let inlineAttachmentObject = FreshWebhookService.getAttachmentObjectFromUrl(inlineUrl);
					attachments.push(inlineAttachmentObject);
				})
			}
			//console.log("reqBody.attachments", attachments.length, reqBody.attachments);
			if(attachments.length > 0 || (reqBody.attachments && reqBody.attachments.length>2)) {	// here reqBody.attachments will be a string from webhook payload
				freshTicket = await FreshService.getTicket(fsTicketId);
				attachments = attachments.concat(freshTicket.ticket.attachments);
				reqBody.attachments = attachments;
				reqBody.description += this.formatFileNames(attachments, 'ticket', true);
				cwTicketBody = await this.transformToConnectCreate(fsTicketId, reqBody);
			}else{
				cwTicketBody = await this.transformToConnectCreate(fsTicketId, reqBody)
			}
			let fsTicketIdObject = {
				"id": 8,
				"caption": "ExternalID",
				"value": String(fsTicketId)
			};
			cwTicketBody['customFields'] = [fsTicketIdObject];
			let cwTicket = null;
			cwTicket = await CwService.createTicket(cwTicketBody);
			if(cwTicket){
				//console.log("Syncing attachments", attachments.length);
				//Sync attachments
				await TicketIdMappingService.createTicketMapping(fsTicketId, cwTicket.id);
				if(attachments.length > 0 && freshTicket){
					console.log("ticket total Attachments length: ", attachments);
					await this.syncAttachments(freshTicket.ticket.id, cwTicket.id, attachments, 'ticket');
				}

				console.log("Syncing ref ticket id");
				//Add CW ticket id to FS ticket
				let fsUpdateObject = { "helpdesk_ticket" : {"custom_field": {}}};
				fsUpdateObject["helpdesk_ticket"]["custom_field"][FreshConfigService.getV1CustomField('connectwise_ticket_id')] = String(cwTicket.id);
				await FreshService.updateTicketV1(fsTicketId, fsUpdateObject);
				try{
					if(reqBody['type'] == SERVICE_REQUEST){
						await this.syncChildTickets(reqBody);
					}
				}catch(err){
					console.log("error occurred");
				}
				return cwTicket.id;
			}
			else {
				console.log("Ticket undefined. Some error occurred while creating ticket in ConnectWise");
			}
			return null;
		}
		else {
			return null;
		}
	},

	closeCwTicket: async function(fsTicketId){
		let cwTicketId = await TicketIdMappingService.getConnectTicketId(fsTicketId);
		if(cwTicketId){
			let operation = {
					"op": 'replace',
					"path": 'status',
					"value": {"name": 'Closed'}
			}
			await CwService.updateTicket(cwTicketId, [operation]);
		}
	},

	closeChildTickets: async function(parentTicketId){
		let childTickets = await FreshService.getChildTickets(parentTicketId);
		// console.log("Child tickets to be closed:: ", childTickets);
		if(childTickets && childTickets.length>0){
			let promises = [];
			for(var i=0; i<childTickets.length; i++){
				promises.push(this.closeCwTicket(childTickets[i]))
			}
			await Promise.all(promises);
		}
	},

	updateTicket: async function(reqBody){
		let fsTicketId = this.parseTicketId(reqBody.id);
		let cwTicketId = await TicketIdMappingService.getConnectTicketId(fsTicketId);
		if(cwTicketId){
			let cwFieldsToBeUpdated = await this.transformToConnectUpdate(reqBody, fsTicketId);
			console.log("ConnectTicketId: ", cwTicketId);
			console.log("ConnectFieldsToBeUpdated: ", cwFieldsToBeUpdated);
			let cwTicket = await CwService.updateTicket(cwTicketId, cwFieldsToBeUpdated);
			if(reqBody.status && reqBody.status == "Closed"){
				await this.closeChildTickets(fsTicketId);
			}

			return cwTicket;
		}else{
			let createdTicket =  await this.createTicket(reqBody);
			return createdTicket;
		}
		return null;
	},

	changeTicketStatus : async function(reqBody){
		let fsTicketId = this.parseTicketId(reqBody.id);
		let cwTicketId = await TicketIdMappingService.getConnectTicketId(fsTicketId);
		let cwStatusValue = FreshConfigService.getCwStatus(reqBody.status);
		let cwStatus = null;
		if(cwStatusValue){
        	if(cwStatusValue == "Unknown"){
        		console.log("Ignore the status change:: ", cwStatusValue);
        		return null;
        	}else{
        		cwStatus = {"name": cwStatusValue};
        	}
        }else{
        	cwStatus = {"name": "Unknown"};
        }
		if(cwStatus){
			let operation = {
				"op": 'replace',
				"path": 'status',
				"value": cwStatus
			};
			let cwTicket = await CwService.updateTicket(cwTicketId, [operation]);
		}

	},

	getServiceItemString: function(item){
		let output = "";
		if(item && item.requested_item && item.requested_item.requested_item_values){
			let details = item.requested_item.requested_item_values;
			let keys = Object.keys(details);
			output += "\n\n------Service Item------\n"
			keys.forEach(function(key){
				let value = details[key] ? details[key] : "--"
				output += `${key}: ${value}\n`
			});
			output += "---------------------------------\n\n"
		}
		return output;
	},

	transformToConnectCreate : async function(fsTicketId, reqBody){
		if(reqBody['location'] && reqBody.description.indexOf(reqBody['location'])<0){
			reqBody.description += ("\n\n======Location=====\n"+(reqBody['location'])+"\n==================\n\n");
		}
		if(reqBody['type'] == SERVICE_REQUEST){
			let items = await FreshService.getRequestedItemDetail(fsTicketId);
			for(var i=0; i< items.length; i++){
				let itemString = this.getServiceItemString(items[i]);
				reqBody.description += itemString;
			}
		}
		let map = FreshConfigService.getCreatePayloadMap();
		var dataTransform = DataTransform([reqBody], map);
		var connectPayload = dataTransform.transform()[0];

		let teamExists = await CwService.getTeam(reqBody.group_name);
		if(!teamExists){
			connectPayload.team = {"name": "Unknown"};
		}

		if(connectPayload.priority && connectPayload.priority.name){
			connectPayload.priority["id"] = global.connectPrioritiesMap[connectPayload.priority.name];
		}

		let contact = await CwService.getContact(reqBody.requester_email);
		if(contact && contact.id){
			connectPayload.contact = {"id" : contact.id};
		}else{
			if(reqBody['type'] == SERVICE_REQUEST){
				let parentTicketId = await FreshService.getParentTicket(fsTicketId);
				if(parentTicketId){ //parent ticket exists
					let parentTicket = await FreshService.getTicket(parentTicketId);
					if(parentTicket && parentTicket.ticket){
						let requesterId = parentTicket.ticket.requester_id;
						let requesterEmail = await FreshService.getRequesterEmailById(requesterId);
						console.log("PARENT REQUESTER MAIL: ", requesterEmail);
						if(requesterEmail){
							let cwContact = await CwService.getContact(requesterEmail);
							console.log("PARENT REQUESTER MAIL in CW: ", cwContact);
							if(cwContact && cwContact.id){
								connectPayload.contact = {"id" : cwContact.id};
							}
						}
					}
				}
			}
		}
		return connectPayload;
	},

	transformToConnectUpdate : async function(reqBody, fsTicketId){
		let map = FreshConfigService.getCreatePayloadMap();
		var dataTransform = DataTransform([reqBody], map);
		var connectPayload = dataTransform.transform()[0];

		let teamExists = await CwService.getTeam(reqBody.group_name);

		if(!teamExists){
			connectPayload.team = {"name": "Unknown"};
		}
		else if(reqBody.group_name == "Unknown"){
			delete connectPayload.team;
		}

		let lastSetStatusByCw = await TicketDataService.getLastStatus(fsTicketId);
		if(lastSetStatusByCw == reqBody.status && connectPayload.status){
			delete connectPayload.status;
		}
		if(connectPayload.status && connectPayload.status.name){
			await TicketDataService.createOrUpdateStatus(fsTicketId, reqBody.status);
		}

		let lastSetPriorityByCw = await TicketDataService.getLastPriority(fsTicketId);
		if(lastSetPriorityByCw == reqBody.priority_default && connectPayload.priority){
			delete connectPayload.priority;
		}
		if(connectPayload.priority && connectPayload.priority.name){
			connectPayload.priority["id"] = global.connectPrioritiesMap[connectPayload.priority.name];
			await TicketDataService.createOrUpdatePriority(fsTicketId, reqBody.priority_default);
		}
		connectPayload['customerUpdatedFlag'] = true;
		let connectKeys = Object.keys(connectPayload);
		let operations = [];
		connectKeys.forEach(function(cwKey){
			// if(cwKey !== "priority"){
				let operation = {
			        "op": 'replace',
			        "path": cwKey,
			        "value": connectPayload[cwKey]
			    };
			    operations.push(operation);
		    // }
		});
		return operations;
	},

	syncConversations: async function(reqBody){
		console.log('FS_WHS.syncConversations', 'wait for 3 seconds');
		await new Promise(r => setTimeout(r, 3000));

		let fsTicketId = this.parseTicketId(reqBody.id);
		let ticketMapping = await TicketIdMappingService.getTicketMapping("freshservice", fsTicketId);
		if(!ticketMapping){
			console.log("Ticket mapping doesn't exist for FreshService ticket: ", fsTicketId);
			return null;
		}
		let syncedConvos = await EntityDataService.getFreshCreatedConvos(fsTicketId);
		let allConvos = await FreshService.getConversations(fsTicketId);
		if(allConvos && allConvos.conversations){
			let conversations = allConvos.conversations;
			// if(conversations && conversations.length>0){
			// 	let lastConvo = conversations[conversations.length -1];
			// 	if(lastConvo.body_text.search(FreshConfigService.getParentMergeNoteRegex()) == 0){
			// 		return syncedConvos;
			// 	}
			// }
			for(let i=0; i<conversations.length; i++){
				let convo = conversations[i];
				let id = ""+convo.id;
				syncedConvos = await EntityDataService.getFreshCreatedConvos(fsTicketId);
				if(syncedConvos.indexOf(id)<0){
					console.log("FS Conversation Id to be synced: ", id);
					if( convo.body_text.search(FreshConfigService.getChildMergeNoteRegex()) == 0){
						let parentFsTicketId = StringUtils.getFirstNumFromString(convo.body_text);
						let parentCwTicketId = await TicketIdMappingService.getConnectTicketId(parentFsTicketId);
						convo.body_text = convo.body_text.replace(/\d+/, parentCwTicketId);
					}else if(convo.body_text.search(FreshConfigService.getParentMergeNoteRegex()) == 0){
						convo.body_text = convo.body_text.replace("\n            \n            ", " ");
						convo.body_text = convo.body_text.replace("\n\n            ", " ");

						let childFsTicketId = StringUtils.getFirstNumFromString(convo.body_text);
						let childCwTicketId = await TicketIdMappingService.getConnectTicketId(childFsTicketId);
						convo.body_text = convo.body_text.replace(/\d+/, childCwTicketId);
					}else if(convo.body_text.search(FreshConfigService.getParentMergeNoteDeltaRegex()) == 0){
						let childFsTicketId = StringUtils.getFirstNumFromString(convo.body_text);
						let childCwTicketId = await TicketIdMappingService.getConnectTicketId(childFsTicketId);
						convo.body_text = convo.body_text.replace(/\d+/, childCwTicketId);
					}
					let inlineAttachmentUrls = this.getInlineAttachmentUrls(convo.body); // assuming convo.body should be html content
					if(inlineAttachmentUrls && Array.isArray(inlineAttachmentUrls)){
						inlineAttachmentUrls.forEach((inlineUrl)=>{
							let inlineAttachmentObject = FreshWebhookService.getAttachmentObjectFromUrl(inlineUrl);
							convo.attachments.push(inlineAttachmentObject);
						})
					}
					await this.addConnectNote(ticketMapping[0], ticketMapping[1], ticketMapping[2], convo);
				}
			}
			await FreshWebhookService.updateCustomerUpdatedFlag(ticketMapping[2]);
		}
		return syncedConvos;
	},

	formatFileNames(attachments, attachmentType, isMessage) {
		let fileMessages = isMessage ? '\n\n===Attached===\n\n' : '';
		for(let i =0; i < attachments.length; i++){
			let fileName = attachments[i].name;
			let extension = fileName.split(".").pop();
			fileName = fileName.split(extension).join("");
			if(!attachments[i].inline){
				let extension = fileName.split(".").pop();
				let attachId = attachments[i].attachment_url.split("/")[8];
				fileMessages += isMessage ? (i + 1) + ': ' + attachmentType + '-' +  fileName + attachId + "." + extension + '\n' : attachmentType + '-' + fileName + attachId + "." + extension;
			}else{
				fileMessages += isMessage ? (i + 1) + ': ' + attachmentType + '-' +  fileName  + "." + "png" + '\n' : attachmentType + '-' + fileName + "." + extension;
			}
		}
		return fileMessages + (isMessage ? "\n\n===" : '');
	},

	addConnectNote: async function(ticketMappingId, fsTicketId, cwTicketId, fsConversation){
		let createdNote = null;
		let attachmentFlag = fsConversation.attachments.length > 0;
		if(fsConversation.source==0 || fsConversation.source==6){
			//reply, forward
			let isReply = !fsConversation.private;
			let noteBody = isReply ? "===Replied to: " : "===Forwarded to: ";
			noteBody += (fsConversation.to_emails.join(", ") + "===\n\n");
			noteBody += fsConversation.body_text;
			if(attachmentFlag) {
				noteBody += this.formatFileNames(fsConversation.attachments, 'conversation', true);
			}
			createdNote = await CwService.addNote(cwTicketId, noteBody, !isReply);
			if(attachmentFlag) {
				console.log('Adding conversation attachments');
				this.syncAttachments(fsTicketId, cwTicketId, fsConversation.attachments, 'conversation');
			}
		}else{
			//public private note
			let isPrivate = fsConversation.private;
			let noteBody = isPrivate ? "===Private note added===\n\n" : "===Public note added===\n\n";
			noteBody += fsConversation.body_text;
			if(attachmentFlag) {
				noteBody += this.formatFileNames(fsConversation.attachments, 'notes', true);
			}
			createdNote = await CwService.addNote(cwTicketId, noteBody, isPrivate);
			if(attachmentFlag) {
				console.log('Adding note attachments');
				this.syncAttachments(fsTicketId, cwTicketId, fsConversation.attachments, 'notes');
			}
		}

		if(createdNote){
			console.log("FS Note: added in CW ", createdNote.id);
			await EntityDataService.addFreshConvo(ticketMappingId, fsConversation.id, createdNote.id);
		}
	},

	syncAttachments: async function(freshTicketId, cwTicketId, attachments, attachmentType){
		if(attachments){
			for(let i =0; i<attachments.length; i++){
				let file = attachments[i];
				try {
					let filePath = global.appRoot+'/dist/File_'+FreshConfigService.getFileNamePrefix()+freshTicketId+"_"+i+"_"+file.name;
					await FileUtils.downloadFile(file.attachment_url, filePath);
					let cwFile = await CwService.addAttachment(cwTicketId, fs.createReadStream(filePath), this.formatFileNames([file], attachmentType, false));
					// let mapping = await TicketIdMappingService.getTicketMapping('freshservice', freshTicketId);
					// console.log(cwFile, file);
					// let output = await EntityDataService.addFreshAttachments(mapping[0], file.id, cwFile.id);
					} catch(error){
					console.log("Error occurred while add file on trac", error);
				}
			}
		}
	},

	createNoteOnRequestStatusChange: async function(ticketId, ticketStatus, noteBody, isPrivate) {
		let fsTicketId = this.parseTicketId(ticketId);
		let cwTicketId = await TicketIdMappingService.getConnectTicketId(fsTicketId);
		console.log('FS Ticket ID', fsTicketId, 'CW Ticket ID', cwTicketId);
		if(cwTicketId){
			let createdCWNote = await CwService.addNote(cwTicketId, noteBody, isPrivate);
			if(ticketStatus){
				await CwService.updateTicketStatus(cwTicketId, ticketStatus);
				let freshServiceStatus = CwConfigService.getFreshServiceStatusStr(ticketStatus);
				if(freshServiceStatus){
					await TicketDataService.createOrUpdateStatus(fsTicketId, freshServiceStatus);
					await FreshService.updateTicketType(fsTicketId, SERVICE_REQUEST, freshServiceStatus);
				}
			}
			return {"Connectwise Note": createdCWNote};
		}else{
			console.log("FreshService - Connectwise ticket mapping doesn't exist");
		}
		return null;
	},

	syncChildTickets: async function(reqBody){
		let fsTicketId = this.parseTicketId(reqBody.id);
		let childTickets = await FreshService.getChildTickets(fsTicketId);
		if(childTickets && childTickets.length>0){
			let parentTicketMapping = await TicketIdMappingService.getTicketMapping("freshservice", fsTicketId);
			if(parentTicketMapping){
				for(let i=0; i<childTickets.length; i++){
					let fsChildTicketId = childTickets[i];
					let childTicketMapping = await TicketIdMappingService.getTicketMapping("freshservice", fsChildTicketId);
					if(childTicketMapping){
						let isMergeEntryExist = await EntityDataService.isMergeEntryExist(parentTicketMapping[0], "MERGE_"+childTicketMapping[1], "MERGE_"+childTicketMapping[2]);
						console.log("Merge entry exists: ", isMergeEntryExist);
						if(!isMergeEntryExist){
							//Add note to parent
							let parentNoteBody = "===Ticket Merged===\n\n";
							parentNoteBody += ("Ticket "+childTicketMapping[2]+" was added as a child to this ticket.")
							let createdParentNote = await CwService.addNote(parentTicketMapping[2], parentNoteBody, true);
							if(createdParentNote){
								console.log("FS Note: added in CW ", createdParentNote.id);
								await EntityDataService.addFreshConvo(parentTicketMapping[0], "MERGE_"+childTicketMapping[1], createdParentNote.id);
							}

							// Add note to child
							let childNoteBody = "===Ticket Merged===\n\n";
							childNoteBody += ("Ticket "+parentTicketMapping[2]+" was set as parent of this ticket.")
							let createdChildNote = await CwService.addNote(childTicketMapping[2], childNoteBody, true);
							if(createdChildNote){
								console.log("FS Note: added in CW ", createdChildNote.id);
								await EntityDataService.addFreshConvo(childTicketMapping[0], "MERGE_"+childTicketMapping[1], createdChildNote.id);
							}
						}else{
							console.log(`Merge entry already exists for ${fsTicketId} and childTicketId: ${fsChildTicketId}`);
						}
					}

				}
			}
		}
	},

	updateCustomerUpdatedFlag: async function(cwTicketId){
		let operation = {
				"op": 'replace',
				"path": 'customerUpdatedFlag',
				"value": true
		};
		await CwService.updateTicket(cwTicketId, [operation]);

	}

};

module.exports = FreshWebhookService;
