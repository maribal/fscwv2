
let DataTransform = require("node-json-transform").DataTransform;

let TicketIdMappingService = require("./TicketIdMappingService");
let FreshService = require("../../freshservice/service/FreshService");
let FreshConfigService = require("../../freshservice/service/FreshConfigService");
let EntityDataService = require("../../commons/service/EntityDataService");
let TicketDataService = require("../../commons/service/TicketDataService");
let CwConfigService = require("../../connectwise/service/ConnectWiseConfigService");
let CwService = require("../../connectwise/service/ConnectWiseService");

const WAITING_FOR_APPROVAL = "Waiting For Approval";
const SERVICE_REQUEST = "Service Request";
const INCIDENT = "Incident";

let ConnectWebhookService = {

	createTicket: async function(reqBody) {
		try {
			console.log("\n\n Connect webhookHandler, createTicket\n\n");
			console.log(JSON.stringify(reqBody));
		} catch (err2) {}
		let cwTicketId = reqBody.id;
		let ticketMapping = await TicketIdMappingService.getTicketMapping("connectwise", cwTicketId);
		if(!ticketMapping) {
			try {
				let originalTicketBody = await CwService.getTicket(cwTicketId);
				reqBody["summary"] = originalTicketBody["summary"];
				let freshServicePayload = await this.transformToFreshCreate(reqBody, true);
				freshServicePayload =  CwConfigService.addCWTicketIdToFreshPayload(freshServicePayload, cwTicketId, true);
				let freshServiceTicket = await FreshService.createTicket(freshServicePayload);
				// console.log("freshServiceTicket", freshServiceTicket);
				if(freshServiceTicket){
					let createdFreshTicketId = freshServiceTicket.item.helpdesk_ticket.display_id
					await TicketIdMappingService.createTicketMapping(createdFreshTicketId, reqBody.id);
					console.log('Created Freshservice ticket with ID: ' + createdFreshTicketId);

					await this.updateTicketStatusInDb(createdFreshTicketId, reqBody.status);
					await this.updateTicketPriorityInDb(createdFreshTicketId, reqBody.priority);

					//Update externalId
					let cwTicket = await CwService.getTicket(cwTicketId);
					await this.updateExternalId(cwTicket, createdFreshTicketId);
				}else{
					console.log(`OOps! Ticket occurred while creating a ticket in Freshservice. CwTicketId ${cwTicketId}`);
				}
			}
			catch(e) {
				console.log('Error occured while creating FS Ticket');
				console.log(e);
			}
		}
		else {
			console.log('Ticket already exists');
		}
	},

	updateTicket: async function(reqBody){
		try {
			console.log("\n\n Connect webhookHandler, updateTicket\n\n");
			console.log(JSON.stringify(reqBody));
		} catch (err2) {}
		let cwTicketId = reqBody.id;
		let ticketMapping = await TicketIdMappingService.getTicketMapping("connectwise", cwTicketId);
		if(!ticketMapping){
			console.log("Ticket mapping doesn't exist for ConnectWise ticket: ", cwTicketId);
			return null;
		}
		let freshTicketId = ticketMapping[1];
		if(freshTicketId){
			let originalTicketBody = await CwService.getTicket(cwTicketId);
			reqBody["summary"] = originalTicketBody["summary"];

			let freshServiceTicket = await FreshService.getTicket(freshTicketId);
			let currentTicketType = freshServiceTicket.ticket.type;

			let freshFieldsToBeUpdated = await this.transformToFreshUpdate(reqBody, true, freshServiceTicket);
			if(reqBody.status && reqBody.status.name && reqBody.status.name == WAITING_FOR_APPROVAL && currentTicketType != SERVICE_REQUEST){
				freshFieldsToBeUpdated['helpdesk_ticket']['ticket_type'] = SERVICE_REQUEST;
			}
			// console.log("Service Request FreshFieldsToBeUpdated: ", freshFieldsToBeUpdated['helpdesk_ticket']['due_by'], freshServiceTicket.ticket.fr_due_by);
			if(freshFieldsToBeUpdated['helpdesk_ticket']['due_by'] && freshServiceTicket.ticket.fr_due_by){
				freshFieldsToBeUpdated['helpdesk_ticket']['frDueBy'] = freshServiceTicket.ticket.fr_due_by;
			}
			let updatedTicket = await FreshService.updateTicketV1(freshTicketId, freshFieldsToBeUpdated);
			await this.updateTicketStatusInDb(freshTicketId, reqBody.status);
			await this.updateTicketPriorityInDb(freshTicketId, reqBody.priority);

			let syncedConvos = await EntityDataService.getConnectCreatedConvos(cwTicketId);
			await this.syncConversations(cwTicketId, ticketMapping, syncedConvos);
			await this.syncDocuments(ticketMapping[0], ticketMapping[1], cwTicketId, syncedConvos);
			await this.syncTimeEntries(ticketMapping[0], ticketMapping[1], cwTicketId, syncedConvos);
			if(reqBody.hasChildTicket){
				await this.syncChildTickets(ticketMapping[0], ticketMapping[1], cwTicketId, syncedConvos);
			}
			return updatedTicket;
		}else{
			console.error("Ticket mapping doesn't exist for CwTicketId:: ", cwTicketId);
		}
		return null;
	},

	updateExternalId: async function(cwTicket, externalId){
		let found = false;

		if (cwTicket && externalId){
			let customFields = cwTicket.customFields;

			for (let i=0; i<customFields.length; i++){
				if (customFields[i].caption === 'ExternalID'){
					customFields[i]['value'] = String(externalId);

					found = true;
				}
			}

			const operation = {
		        'op': 'replace',
		        'path': 'customFields',
		        'value': customFields
		    };

			if (found) {
				return await CwService.updateTicket(cwTicket.id, [operation]);
			}
		}

		return null;
	},

	updateTicketStatusInDb: async function(fsTicketId, cwStatus){
		if (cwStatus && cwStatus.name){
			const fsStatus = CwConfigService.getFreshServiceStatusStr(cwStatus.name);

			if (fsStatus){
				return await TicketDataService.createOrUpdateStatus(fsTicketId, fsStatus);
			}
		}

		return null;
	},

	updateTicketPriorityInDb: async function(fsTicketId, cwPriority){
		if (cwPriority && cwPriority.name){
			const fsPriority = CwConfigService.getFreshServicePriorityStr(cwPriority.name);

			if (fsPriority){
				return await TicketDataService.createOrUpdatePriority(fsTicketId, fsPriority);
			}
		}

		return null;
	},

	transformToFreshCreate : async function(reqBody, isApiV1=false){
		let freshServicePayload = await this.transformToFreshUpdate(reqBody, isApiV1);
		let requester = await FreshService.createRequesterIfNotExist(reqBody.contactName, reqBody.contactEmailAddress, reqBody.contactPhoneNumber);
		let requesterEmail = (requester) ? reqBody.contactEmailAddress : FreshConfigService.getApiUser();
		if(isApiV1){
			freshServicePayload['helpdesk_ticket']['email'] = requesterEmail;
			if(!(freshServicePayload.helpdesk_ticket.group_id)){
				freshServicePayload.helpdesk_ticket['group_id'] = global.unknownGroupId;
			}

			if(!(freshServicePayload.helpdesk_ticket.status)) {
				freshServicePayload.helpdesk_ticket['status'] = 2;
			}
		}else{
			freshServicePayload['email'] = requesterEmail;
			if(!(freshServicePayload.group_id)){
				freshServicePayload['group_id'] = global.unknownGroupId;
			}

			if(!(freshServicePayload.status)){
				freshServicePayload['status'] = 2;
			}
		}
		return freshServicePayload ;
	},


	transformToFreshUpdate : async function(reqBody, isApiV1=false, currentFreshServiceTicket=null){
		try {
			console.log("\n\n Connect webhookHandler, transformToFreshUpdate\n\n");
			console.log(JSON.stringify(reqBody));
		} catch (err2) {}
		let map = null;
		if(isApiV1){
			map = CwConfigService.getUpdateV1PayloadMap();
		}else{
			map = CwConfigService.getUpdatePayloadMap();
		}
		var dataTransform = DataTransform([reqBody], map);
		var freshPayload = dataTransform.transform()[0];


		/* freshPayload['responder_id'] = null;
		if(reqBody.owner && reqBody.owner.id){
			let memberEmail = await CwService.getMemberEmailById(reqBody.owner.id);
			if(memberEmail){
				let fsMemberId = await FreshService.getMemberIdByEmail(memberEmail);
				if(fsMemberId){
					freshPayload['responder_id'] = fsMemberId;
				}
			}
		} */

		if(reqBody.owner && reqBody.owner.id){
			if(isApiV1){
				let memberName = await CwService.getMemberNameById(reqBody.owner.id);
				memberName = memberName ? memberName : "";
				freshPayload['custom_field'][FreshConfigService.getV1CustomField('cw_agent')] = memberName;
			}
		}

		if(currentFreshServiceTicket && !currentFreshServiceTicket.ticket.responder_id && global.defaultAgentId){
			freshPayload['responder_id'] = global.defaultAgentId;
		}

		if(reqBody.team && reqBody.team.name && reqBody.team.name != "Unknown"){
			let groupId = await FreshService.getGroupIdByName(reqBody.team.name);
			console.log("groupId: ", groupId);
			if(groupId){
				freshPayload['group_id'] = groupId;
			}else{
				freshPayload['group_id'] = global.unknownGroupId;
			}
		}

		if(isApiV1){
			const v1FreshPayload = {"helpdesk_ticket": freshPayload};

			return v1FreshPayload;
		}

		return freshPayload;
	},

	syncConversations: async function(cwTicketId, ticketMapping, syncedConvos) {
		const updatedConvs = [];

		console.log("CW Syncing conversations");
		let allConvos = await CwService.getNotes(cwTicketId);

		console.log("CW syncedConvos: ", syncedConvos);

		if (allConvos) {
			let conversations = allConvos;
			for(let i=1; i<conversations.length; i++){
				let convo = conversations[i];
				let id = ""+convo.id;
				syncedConvos = await EntityDataService.getConnectCreatedConvos(cwTicketId);

				if (syncedConvos.indexOf(id)<0){

					console.log("FS Conversation Id to be synced: ", id);

					if (convo.member && convo.member.id) {
						// get email from CW by member.id
						const getUser = await CwService.getMemberById(convo.member.id);

						console.log('CW, syncConversations, getUser', JSON.stringify(getUser));

						if (getUser && getUser.officeEmail) {
							let getFreshserviceUser = await FreshService.getRequesterByEmail(getUser.officeEmail);

							console.log('CW, syncConversations, getFreshserviceUser', JSON.stringify(getFreshserviceUser));

							if (!getFreshserviceUser) {
								// build name
								let fullName = getUser.firstName;

								if (getUser.lastName) {
									fullName += ` ${ getUser.lastName }`;
								}

								console.log('CW, syncConversations, fullName', fullName);

								getFreshserviceUser = await FreshService.createRequester(fullName, getUser.officeEmail, getUser.officePhone);

								console.log('CW, syncConversations, getFreshserviceUser, 2', JSON.stringify(getFreshserviceUser));
							}

							if (getFreshserviceUser && getFreshserviceUser.id) {
								// change convo
								convo.userId = getFreshserviceUser.id;
							}
						}

						console.log('CW, syncConversations, convo', JSON.stringify(convo));

						convo.text = `${ convo.member.name } added a note in ConnectWise at ${ convo.dateCreated }: \n\n ${ convo.text }`;
					}

					const addConv = await this.addConnectNote(ticketMapping[0], ticketMapping[1], convo);

					if (addConv) {
						updatedConvs.push(addConv);
					}
				}
			}
		}

		return updatedConvs;
	},

	syncTimeEntries: async function(ticketMappingId, fsTicketId, cwTicketId, syncedConvos){
		const timeEntries = [];

		let allTimeEntryNotes = await CwService.getTimeEntryNotes(cwTicketId);
		console.log("\n\n\nsyncTimeEntries.syncedConvos:: ", syncedConvos);

		if (allTimeEntryNotes){
			for(let i=0; i<allTimeEntryNotes.length; i++){
				let convo = allTimeEntryNotes[i];
				let timeEntryId = "TIME_NOTE_"+convo.id;
				syncedConvos = await EntityDataService.getConnectCreatedConvos(cwTicketId);

				if (syncedConvos.indexOf(timeEntryId)<0) {
					let cwNote = {
						"id": timeEntryId,
						"text": convo.notes,
						"internalAnalysisFlag": convo.addToInternalAnalysisFlag,
						attachments: []
					}

					if (convo.member && convo.member.id) {
						// get email from CW by member.id
						const getUser = await CwService.getMemberById(convo.member.id);

						console.log('CW, syncTimeEntries, getUser', JSON.stringify(getUser));

						if (getUser && getUser.officeEmail) {
							let getFreshserviceUser = await FreshService.getRequesterByEmail(getUser.officeEmail);

							console.log('CW, syncTimeEntries, getFreshserviceUser', JSON.stringify(getFreshserviceUser));

							if (!getFreshserviceUser) {
								// build name
								let fullName = getUser.firstName;

								if (getUser.lastName) {
									fullName += ` ${ getUser.lastName }`;
								}

								console.log('CW, syncTimeEntries, fullName', fullName);

								getFreshserviceUser = await FreshService.createRequester(fullName, getUser.officeEmail, getUser.officePhone);

								console.log('CW, syncTimeEntries, getFreshserviceUser, 2', JSON.stringify(getFreshserviceUser));
							}

							if (getFreshserviceUser && getFreshserviceUser.id) {
								// change convo
								cwNote.userId = getFreshserviceUser.id;
							}
						}

						console.log('CW, syncTimeEntries, convo', JSON.stringify(convo));

						convo.notes = `${ convo.member.name } added a note in ConnectWise at ${ convo.dateEntered }: \n\n ${ convo.notes }`;
					}

					// console.log("cwnote", cwNote);
					const timeEntry = await this.addConnectNote(ticketMappingId, fsTicketId, cwNote);

					if (timeEntry) {
						timeEntries.push(timeEntry);
					}
				}
			}
		}

		return timeEntries;
	},

	syncDocuments: async function(ticketMappingId, fsTicketId, cwTicketId, syncedConvos) {
		const docs = [];

		console.log("Syncing documents");
		syncedConvos = await EntityDataService.getConnectCreatedConvos(cwTicketId);
		let attachments = await CwService.getAttachments(cwTicketId);
		if(attachments){
			let syncedConvosStr = syncedConvos.join("#");
			let attachmentsToBeAdded = [];
			let mohinganDocId = null;
			for(let i=0; i<attachments.length; i++){
				let attachment = attachments[i];
				let docId = "Doc_"+attachment.id;
				let filename = attachment._info.filename;
				syncedConvos = await EntityDataService.getConnectCreatedConvos(cwTicketId);
				if(syncedConvosStr.indexOf(docId)<0
					&& filename.indexOf(FreshConfigService.getFileNamePrefix())<0 ){
					let attachmentData = {
						"name": attachment._info.filename,
						"url": CwConfigService.getDocumentDownloadUrl(attachment.id, attachment._info.filename)
					}
					mohinganDocId = ((mohinganDocId) ? (mohinganDocId+"_"+docId) : docId);
					attachmentsToBeAdded.push(attachmentData);
				}
			}
			if(mohinganDocId && attachmentsToBeAdded && attachmentsToBeAdded.length>0){
				let cwNote = {
					"id": mohinganDocId,
					"text": "Documents",
					"internalAnalysisFlag": true,
					"attachments": attachmentsToBeAdded
				}

				const doc = await this.addConnectNote(ticketMappingId, fsTicketId, cwNote);

				if (doc) {
					docs.push(doc);
				}
			}else{
				console.log("attachmentsToBeAdded is 0");
			}
		}

		return docs;
	},

	syncChildTickets: async function(ticketMappingId, fsTicketId, cwTicketId, syncedConvos){
		const updatedChildTickets = [];

		console.log("Syncing childTickets");
		let childTickets = await CwService.getChildTickets(cwTicketId);

		if (childTickets && childTickets.length>0) {
			for(let i=0; i<childTickets.length; i++){
				let childTicket = childTickets[i];
				let id = "MERGE_"+childTicket.childTicketId;

				let fsChildTicketMapping = await TicketIdMappingService.getTicketMapping("connectwise", childTicket.childTicketId);
				if(fsChildTicketMapping){
					let isMergeEntryExist = await EntityDataService.isMergeEntryExist(ticketMappingId, "MERGE_"+fsChildTicketMapping[1], id);
					if(!isMergeEntryExist){
						//Note to parentTicket
						let parentCwNote = {
							"id": id,
							"text": `Ticket ${fsChildTicketMapping[1]} was added as a child to this ticket.`,
							"internalAnalysisFlag": true
						}
						const forParent = await this.addConnectNote(ticketMappingId, fsTicketId, parentCwNote);

						if (forParent) {
							childTicket.push(`Merge note for parent ${ forParent }, ticketMappingId=${ ticketMappingId }`);
							console.log("Merge note created for parent ticketMappingId", ticketMappingId);
						}


						//Note to child ticket
						let childCwNote = {
							"id": id,
							"text": `Ticket ${fsTicketId} was set as parent of this ticket`,
							"internalAnalysisFlag": true
						};
						const forChild = await this.addConnectNote(fsChildTicketMapping[0], fsChildTicketMapping[1], childCwNote);

						if (forChild) {
							childTicket.push(`Merge note for child ${ forChild }, ticketMappingId=${ fsChildTicketMapping[0] }`);
							console.log("Merge note created child for ticketMappingId", fsChildTicketMapping[0]);
						}
					}
				}else{
					console.log("Child ticket mapping doesn't exist for CW Ticket: ", childTicket.childTicketId);
				}
			}
		}

		return updatedChildTickets;
	},

	addConnectNote: async function(ticketMappingId, fsTicketId, cwNote){
		let createdNote = null;
		console.log("CW Note to be added: fsTicketId ", fsTicketId);
		//public private note
		let isPrivate = cwNote.internalAnalysisFlag;
		createdNote = await FreshService.addNote(fsTicketId, cwNote.text, isPrivate, cwNote.attachments, cwNote.userId || null);

		if (createdNote){
			console.log("CW Note: added in Freshservice", createdNote.conversation.id);
			return await EntityDataService.addFreshConvo(ticketMappingId, createdNote.conversation.id, cwNote.id);
		} else{
			console.log("CW Note: addition failed in Freshservice");
		}

		return null;
	},
};

module.exports = ConnectWebhookService;
