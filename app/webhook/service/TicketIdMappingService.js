
let TicketIdDataProvider = require('../dataprovider/TicketIdDataProvider');

let TicketIdMappingService = {

    createTicketMapping: async function(fsTicketId, cwTicketId){
		await TicketIdDataProvider.createMapping(fsTicketId, cwTicketId);

		return 'Successfully created the ticket mapping !!';
	},

	getFreshTicketId: async function(cwTicketId){
		let fields = await TicketIdDataProvider.getFreshTicketId(cwTicketId);
		if(fields && fields.length>0){
			return fields[0].freshservice_ticket_id;
		}
		return null;
	},

	getTicketMapping: async function(source, ticketId){
		let fields = await TicketIdDataProvider.getMapping(source, ticketId);
		if(fields && fields.length>0){
			return [fields[0].id, fields[0].freshservice_ticket_id, fields[0].connectwise_ticket_id];
		}
		return null;
	},

	getConnectTicketId: async function(fsTicketId){
		let fields = await TicketIdDataProvider.getConnectTicketId(fsTicketId);

		if(fields && fields.length>0){
			return fields[0].connectwise_ticket_id;
		}
		return null;
	}

}

module.exports = TicketIdMappingService;
