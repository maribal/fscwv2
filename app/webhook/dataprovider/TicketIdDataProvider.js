
let db = require('../../../dbmanager/mysql/db')

/*
 * Data provider is the interaction layer for DB. 
 * Only data provider can read/write to DB directly and this will contain all raw queries.
 */

 let TicketIdDataProvider = {

    createMapping: function(freshdeskTicketId, tracTicketId){
        let query = "INSERT INTO ticket_id_mappings (freshservice_ticket_id, connectwise_ticket_id) VALUES (?, ?)";
        return db.querySql(query, [freshdeskTicketId, tracTicketId]);
    },

    getFreshTicketId: function(cwTicketId){
        let query = "SELECT freshservice_ticket_id FROM ticket_id_mappings WHERE connectwise_ticket_id = ? LIMIT 1";
        return db.querySql(query, [cwTicketId]);
    },

    getConnectTicketId: function(fsTicketId){
        let query = "SELECT connectwise_ticket_id FROM ticket_id_mappings WHERE freshservice_ticket_id = ? LIMIT 1";
        return db.querySql(query, [fsTicketId]);
    },

    getMapping: function(source, ticketId) {
        const columnName = source == 'freshservice' ? 'freshservice_ticket_id' : 'connectwise_ticket_id';
        let query = "SELECT * FROM ticket_id_mappings WHERE " + columnName + " = ?";
        return db.querySql(query, [ticketId]);
    }
 }

module.exports = TicketIdDataProvider
