'use strict';

let { success, error } = require("../../../utils/ResponseWrapperUtils");
let { notNull, isNull } = require("../../../utils/ObjectUtils");
let FreshWebhookService = require("../service/FreshWebhookService");
let FreshConfigService = require("../../freshservice/service/FreshConfigService")
const StringUtils = require("../../../utils/StringUtils");

const CW_WAITING_FOR_APPROVAL = "Waiting For Approval";
const CW_APPROVED = "CR-Approved";
const CW_DECLINED = "CR-Declined";

let FreshWebhookController = {
	createTicket: (req, res) => {
		console.log("\n\n FreshService webhookHandler\n\n");
		// console.log(req.body);
		// return success(res);
		try{
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)){
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			// console.log(req.body);
			FreshWebhookService.createTicket(req.body)
		    .then((ticketId)=>{
		    	console.log("Ticket created on CW with id: ", ticketId);
		    	return success(res, ticketId)
		    })
		    .catch(err =>{
		    	console.log("Exception occurred: ", err);
		    	return error(res);
		    });
	    }catch(err){
	    	console.log("Exception occurred: ", err);
	    	return error(res);
	    }
	},

	updateTicket: (req, res) => {
		console.log("\n\n FreshService webhookHandler\n\n");
		// console.log(req.body);
		// return success(res);
		try{
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)){
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			// console.log(req.body);
			FreshWebhookService.updateTicket(req.body)
		    .then((ticket)=>{
		    	console.log("Ticket updated on CW");
		    	return success(res, ticket)
		    })
		    .catch(err =>{
		    	console.log("Exception occurred: ", err);
		    	return error(res);
		    });
		}catch(err){
	    	console.log("Exception occurred: ", err);
			return error(res);
	    }
	},

	syncConversation: async (req, res) => {
		console.log("FreshService reply Ticket Handler");
		// console.log("\n\n FreshService webhookHandler\n\n");
		// console.log(req.body);
		try {
			console.log("\n\n FreshService webhookHandler, syncConversation\n\n");
			console.log(JSON.stringify(req.body));
			} catch (err2) {}
		try{
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)){
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			let latestPrivateComment = StringUtils.getTextFromHtml(req.body.latest_private_comment);
			if(latestPrivateComment && latestPrivateComment.search( FreshConfigService.getParentMergeNoteRegex() ) >=0){
				console.log("Discarding this trigger due to prevent double note creation while merging tickets");
				return success(res);
			}
			// console.log(req.body);
			let syncedConvos = await FreshWebhookService.syncConversations(req.body);
		    return success(res, syncedConvos);
		}catch(err){
	    	console.log("Exception occurred: ", err);
			return error(res);
	    }
	},

	requestForApproval: async(req, res) => {
		console.log("\n\n FreshService WebHookHandler for Request for Approval\n\n");
		// console.log(req.body);
		try {
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)) {
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			let createdNotes = await FreshWebhookService
				.createNoteOnRequestStatusChange(req.body.freshdesk_webhook.ticket_id, CW_WAITING_FOR_APPROVAL,"WAITING FOR APPROVAL", true);
			return success(res, createdNotes);
		} catch ( err ) {
			console.log("Exception occurred: ", err);
			return error(res);
		}
	},

	approveRequest: async(req, res) => {
		console.log("\n\n FreshService WebHookHandler for Request Accept\n\n");
		// console.log(req.body);
		try {
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)) {
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			let createdNotes = await FreshWebhookService
				.createNoteOnRequestStatusChange(req.body.freshdesk_webhook.ticket_id, CW_APPROVED, "APPROVED", true);
			return success(res, createdNotes);
		} catch ( err ) {
			console.log("Exception occurred: ", err);
			return error(res);
		}
	},

	rejectRequest: async(req, res) => {
		console.log("\n\n FreshService WebHookHandler for Request Reject\n\n");
		// console.log(req.body);
		try {
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)) {
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			let createdNotes = await FreshWebhookService
				.createNoteOnRequestStatusChange(req.body.freshdesk_webhook.ticket_id, CW_DECLINED, "REJECTED", true);
			return success(res, createdNotes);
		} catch ( err ) {
			console.log("Exception occurred: ", err);
			return error(res);
		}
	},

	syncChildTickets: async (req, res) => {
		console.log("FreshService reply Ticket Handler");
		try{
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)) {
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			await FreshWebhookService.syncChildTickets(req.body);
		    return success(res);
		}catch(err){
	    	console.log("Exception occurred: ", err);
			return error(res);
	    }
	},

	changeStatus: async(req, res) => {
		try{
			if(!FreshConfigService.isValidTrigger(req.body.event_performer)) {
				console.log("Discarding this trigger. Event Performer: ", req.body.event_performer);
				return success(res);
			}
			await FreshWebhookService.changeTicketStatus(req.body);
		    return success(res);
		}catch(err){
	    	console.log("Exception occurred: ", err);
			return error(res);
	    }
	}

};

module.exports = FreshWebhookController;
