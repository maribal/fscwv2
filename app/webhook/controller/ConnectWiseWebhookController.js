'use strict';

const { success, error } = require("../../../utils/ResponseWrapperUtils");
const { notNull, isNull } = require("../../../utils/ObjectUtils");
const FreshService = require("../../freshservice/service/FreshService");
let ConnectWebhookService = require("../service/ConnectWebhookService");
const log = require('../../../utils/log')('app:webhook:controller:ConnectWise');
const debug = require('debug')('app:webhook:controller:ConnectWise');

let ConnectWiseWebhookController = {
	verifyRequest: (req, res) => {
		try {
			if (! req.body) {
				throw new Error('body');
			}

			if (! ('Action' in req.body) || !["updated", "added"].includes(req.body.Action) ) {
				throw new Error(['Action', ( req.body.Action || 'undefined' )].join(','));
			}

			if (! ('Type' in req.body) || ! req.body.Type) {
				throw new Error('Type');
			}

			if ("ticket" === req.body.Type && ! ('Entity' in req.body)) {
				throw new Error('Entity');
			}

			try {
				payload = JSON.parse(req.body.Entity);
			} catch (ex) {
				throw new Error('Entity');
			}

			return true;
		} catch (err) {
			log.error(err.toString());
			res.status(400).json({
				_ver: "v1",
				success: false,
				message: "wrong_field",
				payload: err.toString()
			});
			return false;
		}
	},
	webhookHandler: async (req, res) => {
		if (! ConnectWiseWebhookController.verifyRequest(req, res)) {
			return false;
		}
		try {
			if(["updated", "added"].includes(req.body.Action) && req.body.Type === "ticket") {
				let payload = JSON.parse(req.body.Entity);
				if (payload._info.updatedBy !== "Alithya") {
					let processedTicket = null;
					if("added" === req.body.Action) {
						 processedTicket = await ConnectWebhookService.createTicket(payload);
					} else if("updated" === req.body.Action ) {
						processedTicket = await ConnectWebhookService.updateTicket(payload);
					} else {
						log.info("Rejecting self updates");
					}
					return success(res, processedTicket);
				} else {
					log.info("Rejecting self updates");
				}
			} else {
				log.info("Rejecting self updates");
			}
	   } catch (err) {
				return error(res);
	   }
	}
};

module.exports = ConnectWiseWebhookController;
