'use strict';

let express = require('express');
let router = express.Router();
let FreshWebhookController = require('./controller/FreshWebhookController');
let ConnectWiseWebhookController = require('./controller/ConnectWiseWebhookController');

router.post('/freshservice/ticket/create', FreshWebhookController.createTicket);
router.post('/freshservice/ticket/update', FreshWebhookController.updateTicket);
router.post('/freshservice/ticket/conversation', FreshWebhookController.syncConversation);
router.post('/freshservice/ticket/childticket', FreshWebhookController.syncChildTickets);

router.post('/freshservice/serviceRequest/requestForApproval', FreshWebhookController.requestForApproval);
router.post('/freshservice/serviceRequest/approve', FreshWebhookController.approveRequest);
router.post('/freshservice/serviceRequest/reject', FreshWebhookController.rejectRequest);

router.post('/connectwise/', ConnectWiseWebhookController.webhookHandler);

module.exports = router;