Freshservice-Connectwise Integraiton

*Steps to start development quickly*
- Clone frsh-alithya Node app
- Ensure that `config/local/db.config.json` MySQL credentials match your local config
- Import `schema.sql`
- Run the app and link localhost:8080 to a ngrok URL
- Register Webhook on Connectwise and Freshservice (cURL for Connectwise and link for Freshservice webhook creator shared above). The above set ngrok URL will be used as the target for Connectwise and Freshservice
- Create a ticket on Freshservice and confirm incoming connection on Node app (logs prints the API payload)
- Update the same ticket (try changing ticket name, status, create a note, reply etc) on Freshservice and confirm incoming connection
- Login to Connectwise and check for the newly created ticket under Service > Tickets section
- Update fields on Connectwise and confirm incoming webhook on local server (logs printed in console)
- Test CRUD actions for the assigned model you're working on (Attachments, Notes, Time log, etc) on POSTMan for Freshservice and Connectwise