'use strict';

const winston = require('winston');
const { createLogger, format, transports } = winston;
const { combine, timestamp, label, prettyPrint } = format;

const debugLog = createLogger({
  level: 'debug',
  format: combine(
    timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: 'tmp/debug.log', level: 'debug', maxsize: 524288000, //5MB
              maxFiles: 5,
              colorize: false })
  ],
  exceptionHandlers: []
});

const infoLog = createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: 'tmp/info.log', level: 'info', maxsize: 524288000, //5MB
              maxFiles: 5,
              colorize: false })
  ],
  exceptionHandlers: []
});

const severityLevelOnly = winston.format(info => {
  info.level = '';
  return info;
});
const accessLog = createLogger({
  level: 'info',
  format: combine(
    severityLevelOnly(),
    winston.format.simple()
  ),
  transports: [
    new winston.transports.File({ filename: 'tmp/access.log', level: 'info', maxsize: 524288000, //5MB
              maxFiles: 5,
              colorize: false })
  ],
  exceptionHandlers: []
});

const errorLog = createLogger({
  level: 'error',
  format: combine(
    timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({ filename: 'tmp/error.log', level: 'error', maxsize: 524288000, //5MB
              maxFiles: 5,
              colorize: false }),
  ],
  exceptionHandlers: []
});

if (global.env === 'production') {
  errorLog.add(new winston.transports.File({
    filename: 'tmp/exceptions.log',
    maxsize: 524288000, //5MB
    maxFiles: 5,
    colorize: false,
    handleExceptions: true
  }));
} else {
  errorLog.add(
    new winston.transports.Console({
      json: false,
      colorize: true,
      handleExceptions: true
    })
  );
}

accessLog.stream = {
    write: function(message, encoding){
        accessLog.info(message);
    }
};

exports = module.exports = createLog;

function createLog(namespace) {

  function info(...args) {
    infoLog.log('info', {namespace: info.namespace, message: args});
  }

  function error(...args) {
    errorLog.log('error', {namespace: error.namespace, message: args});
  }

  function debug(...args) {
    debugLog.log('debug', {namespace: debug.namespace, message: args});
  }

  if (! namespace.startsWith('fscwv')) {
    namespace = 'fscwv:'+namespace;
  }

  info.namespace = namespace;
  error.namespace = namespace;
  debug.namespace = namespace;

  return {
    access_log: accessLog.stream,
    info: info,
    error: error,
    debug: debug
  };
}
