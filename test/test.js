'use strict';

process.env.NODE_ENV = 'local';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);
try {
describe('Webhooks', () => {
  describe('/GET webhook', () => {
    it('it should not work GET', (done) => {
      chai.request(server)
        .get('/api/webhook')
        .end((err, res) => {
          res.should.have.status(404);

          done();
        });
    });
  });

  describe('/POST webhook/connectwise/', () => {
    it('it should not work POST without stuff', (done) => {
      let data = {
      };
      chai.request(server)
        .post('/api/webhook/connectwise/')
        .send(data)
        .end((err, res) => {
          res.should.have.status('400');
          done();
        })
    });
    it('it should not work POST without correct Action ', (done) => {
      let data = {
        Action: "plm"
      };
      chai.request(server)
        .post('/api/webhook/connectwise/')
        .send(data)
        .end((err, res) => {
          res.should.have.status('400');
          done();
        })
    });
    it('it should not work POST without Type, updated===Action ', (done) => {
      let data = {
        Action: "updated"
      };
      chai.request(server)
        .post('/api/webhook/connectwise/')
        .send(data)
        .end((err, res) => {
          res.should.have.status('400');
          done();
        })
    });
    it('it should not work POST without Type, added===Action ', (done) => {
      let data = {
        Action: "added"
      };
      chai.request(server)
        .post('/api/webhook/connectwise/')
        .send(data)
        .end((err, res) => {
          res.should.have.status('400');
          res.body.should.be.a('object');
          res.body.should.have.property('success').eql(true);
          done();
        })
    });
  });
});
} catch (eex) {
  console.log(eex);
}
