'use strict';


let util = require('util');
let express = require('express');
let http = require('http');
let app = express();
let router = express.Router();
let path = require('path');
const debug = require('debug')('fscwv:app');
const log = require('./utils/log')('app');
const morgan = require('morgan');

log.info("====================== Starting");

// disable X-Powered-By
app.disable('x-powered-by');
// Use combined log format: https://github.com/expressjs/morgan output to process.stdout
app.use(morgan('combined', { stream: log.access_log }));
app.use(express.urlencoded({ extended: true }));

// Reading env variables
//console.log("env", process.env);
global.env = process.env.NODE_ENV || 'production';
app.set('port', process.env.PORT || 8080);
global.appRoot = path.resolve(__dirname);
//db setup
let mysql_conn = require('./dbmanager/mysql/connection.js');
mysql_conn.setup();

// middlewares
let middleware = require('./middleware');
var middlewares = Object.keys(middleware).map(function(key) {
    return middleware[key];
});
app.use(middlewares);

app.use(router);
require('./router/')(router);

app.use(function (req, res, next) {
  log.debug({
    headers: req.headers,
    url: req.originalUrl,
    data: req.body,
    query: req.query,
    method: req.method,
    cookies: req.cookies
  });
  next();
})

app.use('*', function(req, res) {
  log.error(req.url);
  res.status(404).send("Page not found!");
});

app.use(function(err, req, res, next) {
  log.error(err.message);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'local' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

let CwService = require('./app/connectwise/service/ConnectWiseService');
let FreshService = require('./app/freshservice/service/FreshService');
let FreshConfigService = require('./app/freshservice/service/FreshConfigService');

// Fetch default connectwise agent id
(async () => {
    try {
        let connectwiseAgentId = await FreshService.getMemberIdByEmail( FreshConfigService.getDefaultAgentEmail() );
        global.defaultAgentId = connectwiseAgentId;
        log.info({"Default Agent Id":global.defaultAgentId});

        let unknownGroupId = await FreshService.getGroupIdByName( FreshConfigService.getUnknownGroupName() );
        global.unknownGroupId = unknownGroupId;
        log.info({"Unknown Group Id": global.unknownGroupId});

        let ticketProperties = await FreshService.getTicketFields();
        global.statusPropsMap = FreshService.getPropertiesMap(ticketProperties, "status");
        global.ticketTypePropsMap = FreshService.getPropertiesMap(ticketProperties, "ticket_type");
        global.priorityPropsMap = FreshService.getPropertiesMap(ticketProperties, "priority");

        global.connectPrioritiesMap = await CwService.getPriorityMap();

        log.info("Freshservice properties are fetched too! Application is ready to serve...");

    } catch (e) {
        log.error("Error occurred while fetching API user id in app.js", e);
    }
})();

var server = http.createServer(app)
  .on('error', function(err) {
    log.error(err);
    process.exit(1);
  })
  .listen(app.get('port'),'0.0.0.0', function() {
    log.info('Effy Server listening on port ' + app.get('port') + ' in ' + (global.env));
  });

module.exports = server;
